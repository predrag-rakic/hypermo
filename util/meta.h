#ifndef _META_H_
#define _META_H_

#include <cstddef>  // size_t

/// Template meta-programming namespace.
namespace meta {

//********************************************************
/// long int -> type (Alexandrescu)
/**
 * Converts particular int value (indistinguishable to the compiler) to 
 * a type (distinguishable to the compiler).
 */
template<long int I>
struct int2type {};


//********************************************************

/// Helper class for function template 'equal()'.
/** Used if values are _not_ equal. */
template<typename T>
struct equal_impl {
    static constexpr bool value = false;
};

/// Helper class for function template equal(). 
/** 
 * Specialization of class template 'equal_impl'.
 * Used if values _are_ equal. 
 */
template<>
struct equal_impl<int2type<0> > {
    static constexpr bool value = true;
    static void implemented() {}
};

/// Compiles only if I and J are equal.
template<int I, int J>
void equal() {
    equal_impl<int2type<I-J> >::implemented(); 
}
/// Returns true if I and J are equal.
template<int I, int J>
constexpr bool is_equal() {
    return equal_impl<int2type<I-J> >::value;
};


//********************************************************

/// Helper class for function template 'less()'.
template<typename T>
struct less_impl {
    static constexpr bool value = false;
};
/// Helper class for function template 'less()'.
template<>
struct less_impl<int2type<true> > {
    static constexpr bool value = true;
    static void implemented() {}
};

/// Compiles only if I is less then J.
template<int I, int J>
void less() {
    less_impl<int2type< I-J<0 > >::implemented(); 
}
/// Returns true if I and J are equal.
template<int I, int J>
constexpr bool is_less() {
    return less_impl<int2type< I-J<0 > >::value; 
}


//********************************************************

/// Represents the end of the list.
class End {};

template<template<typename> class H, typename T> class l;
/// Type pair used to build a list of types.

/** First element of list must be class template
 * Second element of the list can be only another list or class End.
 */ 
template<template<typename> class H, template<typename> class H2, typename T> 
class l<H, l<H2, T> > {
};
template<template<typename> class H>
class l<H, End> {
};

template<typename, typename> struct Reverse_impl;
template<template<typename> class H, typename T, typename OUT>
struct Reverse_impl< l<H, T>, OUT > {
    typedef typename Reverse_impl<T, l<H, OUT> >::type  type;
};
template<typename OUT>
struct Reverse_impl< End, OUT > {
    typedef OUT  type;
};

template<typename > struct Reverse;
/// Reverse position of the elements in the list.
/** Argument must be of l type.
 */
template<template<typename> class H, typename T>
struct Reverse< l<H, T> > {
    typedef typename Reverse_impl<T, l<H, End> >::type  type;
};


template<typename> struct Inherit;
/// Mechanisam for linear class hierarchy creation (linear inheritance).
/** _Last_ element in the list is base of the hierarchy.
 */
template<template<typename> class H, typename T>
struct Inherit< l< H, T > > : public H<Inherit<T> > {
};
template<template<typename> class H>
struct Inherit< l<H, End> > : public H<End>  {
};

/// Similar to Inherit, inherits from class hierarchy defined in the list.
/** Reverses the list so that the _first_ element in the list 
 * is base of the hierarchy.
*/
template<typename T> 
struct ClassHierarchy : public Inherit<typename Reverse<T>::type> {
};


//****************************************
template <size_t...>
struct indices_pack {};


template <size_t Sp, class IntPack, size_t Ep> struct make_indices_imp;

template <size_t Sp, size_t ... Indices, size_t Ep>
struct make_indices_imp<Sp, indices_pack<Indices...>, Ep>
{
    typedef typename make_indices_imp<Sp+1, indices_pack<Indices..., Sp>, Ep>::type type;
};

template <size_t Ep, size_t ... Indices>
struct make_indices_imp<Ep, indices_pack<Indices...>, Ep>
{
    typedef indices_pack<Indices...> type;
};

/// Generate unsigned sequence for from/to 'parameter pack' conversion.
/**
 * Examples: make_indices_pack<5>::type -> indices_pack<0,1,2,3,4> ; 
 * make_indices_pack<5,2>::type -> indices_pack<2,3,4> ;
 */
template <size_t Ep, size_t Sp = 0>
struct make_indices_pack
{
    // Ne znam kako da napravim da ovaj assert radi.
    static_assert(Sp <= Ep, "make_indices_pack input error"); 
    typedef typename make_indices_imp<Sp, indices_pack<>, Ep>::type type;
};


} //namespace meta

#endif
