#ifndef _LOGGER_H_
#define _LOGGER_H_

#include<iostream>


#ifdef DEBUG
#define hy_log_exec(loglevel, exec) if(loglevel<=DEBUG) { exec; }
#define hy_log(loglevel, poruka) log_exec(loglevel, std::cerr << poruka)
#else
#define hy_log_exec(loglevel, exec) 
#define hy_log(loglevel, poruka) 
#endif

#endif

