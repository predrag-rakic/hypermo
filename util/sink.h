#ifndef _OUT_STREAM_H_
#define _OUT_STREAM_H_

#include <iostream>
#include <fstream>
#include <cstdio>        // std::snprintf()

/// Utility clases and functions
namespace util {
    
/// Simulation outputs are saved through this function
/*
 *
 */
template<typename T>
void sink(std::ostream& os, ///< Output stream 
	  const long seed,  ///< Seed used for this simulation path
	  const T& t        ///< SO structure {double energy, std::array<double,N> magnetisation}
) {
        const int lineLength = 100;
        char line[lineLength];
        // long max 19 digits; double max 17 digits
        snprintf(line, lineLength, "%20ld %24.16e", seed, t.energy);
        os << line;
        for(auto i=0u; i<t.magnetisation.size(); ++i) {
            snprintf(line, lineLength, "%25.16e", t.magnetisation[i]);
            os << line;
        }
        os << std::endl;
}

} // namespace

#endif
