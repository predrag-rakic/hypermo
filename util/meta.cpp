#include<iostream>
#include"meta.h"

using std::cout;
using std::endl;

template<typename BASE>
struct A { 
    int a; 
    int f() { return a; } 
    A() : a(0) {}
};

template<typename BASE>
struct B : public BASE {
    int  f2() { ++BASE::a; return BASE::f() ; } 
};

template<typename BASE>
struct C : public BASE {
    int f3() { ++BASE::a; return BASE::f2(); }
};

//class L;
using meta::l;
typedef l<B, l<A, meta::End> >  List;
typedef meta::Inherit< List >   TT;
typedef l<A, l<B, l<C, meta::End> > > ListR;

class L : public meta::ClassHierarchy<ListR> {
public:
    L() {}
    void run() {
        cout << "a:" << a << endl;
        cout << "f2:" << f2() << endl;
        cout << "f3:" << f3() << endl;
    }
};


int main() {
    meta::less<2,5>();
//    meta::less<5,3>();
//    meta::less<5,5>();

    L l;
    l.run();
    l.run();
    l.run();
}
