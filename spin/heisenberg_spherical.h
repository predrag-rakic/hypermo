// Copyright 2013 Predrag Rakic
// predrag.rakic@gmail.com

#ifndef _HEISENBERG_SPHERICAL_H_
#define _HEISENBERG_SPHERICAL_H_

#include <array>
#include <cmath>  // sin(), cos(), M_PI
#include <numeric>  // inner_product()
#include <algorithm>  // transform()
#include "heisenberg_base.h"
#include "../util/meta.h"

namespace spin {

/// Coordinate system used for spin representation.
/**
 * 
 * @param angle_type    type of angle-value
 * @param N             # of spin axes (dimensions);   N-1 == # of angles 
 * 
 */
template<typename angle_type, size_t N>
struct HeisenbergSpherical : std::array<angle_type, N-1> {

	explicit HeisenbergSpherical(const std::array<angle_type, N-1>& values) 
		:	 std::array<angle_type, N-1> (values) 
	{}
};


/// Totaly random initial state (T=inf).
/**
 * 
 * Spherical spin initialisatin class is expected to provide 
 * static function 'spin_init'
 * that receives 'constructor_arguments_number()' number of float arguments.
 * 
 * @param angle_type    type of angle-value
 * @param N             # of spin axes (dimensions);   N-1 == # of angles
 * 
 */
template<typename angle_type, size_t N>
struct HeisenbergSphericalInitRandom {
	/// Number of random numbers that caller should provide to constructor
	static constexpr size_t constructor_arguments_number() { return N-1; }	
    typedef std::array<float, N-1> Rnd_numbers;

    /// Array initialization 
    /**
     * 
     * @param rnd_numbers array of random numbers of float type,
     *        each used for initialisation of one spin angle.
     * 
     */
    static std::array<angle_type, N-1> spin_init(const Rnd_numbers& rnd_numbers) {
		std::array<angle_type, N-1> angles;
		angles[0] = M_PI * rnd_numbers[0];
		std::transform( rnd_numbers.begin()+1, rnd_numbers.end(),
						angles.begin()+1,
						[](float rnd){
							return static_cast<angle_type>(2*M_PI * rnd);
						}
		);
		return angles;
    }
};

/// Totaly random initial state (T=inf). Specialisation for N==2.
/**
 * 
 * Spherical spin initialisatin class is expected to provide 
 * static function 'spin_init'
 * that receives 'constructor_arguments_number()' number of float arguments.
 * 
 * @param angle_type    type of angle-value
 * 
 */
template<typename angle_type>
struct HeisenbergSphericalInitRandom<angle_type, 2> {
    /// Number of random numbers that caller should provide to constructor
    static constexpr size_t constructor_arguments_number() { return 1; }	
    typedef std::array<float, constructor_arguments_number()> Rnd_numbers;

    /// Array initialization 
    /**
     * 
     * @param rnd_numbers array of random numbers of float type,
     *        each used for initialisation of one spin angle.
     * 
     */
    static std::array<angle_type, 1> spin_init(const Rnd_numbers& rnd_numbers) {
        return {{ static_cast<angle_type>(2*M_PI * rnd_numbers[0]) }};
    }
};

/// Totaly ordered initial state (T=0).
/**
 * 
 * Spherical spin initialisatin class is expected to provide 
 * static function 'spin_init'
 * that receives 'constructor_arguments_number()' number of float arguments.
 * 
 * @param angle_type    type of angle-value
 * @param N             # of spin axes (dimensions);   N-1 == # of angles
 * 
 */
template<typename angle_type, size_t N>
struct HeisenbergSphericalInitUp{
    /// Number of random numbers that caller should provide to constructor
    /**
     * 
     * One angle is always zero --> there should be one rnd number less 
     * than in random init state.
     * 
     */
    static constexpr size_t constructor_arguments_number() { return N-1-1; }	

    typedef std::array<float, constructor_arguments_number()> Rnd_numbers;

    /// Array initialization 
    /**
     * 
     * @param rnd_numbers array of random numbers of float type (size N-2).
     *        Each element is used for initialisation of one spin angle
     * 		  and spin-angle[0] is initialised to 0.
     * @note  When spin-angle[0] is 0 projections on all but one axis are 0.
     * 
     */
    static std::array<angle_type, N-1> spin_init(const Rnd_numbers& rnd_numbers) {
		std::array<angle_type, N-1> angles;
		angles[0] = 0.;
		std::transform( rnd_numbers.begin(), rnd_numbers.end(),
						angles.begin()+1,
						[](float rnd){
							return 2*M_PI * rnd;
						}
		);
        return angles;
    }
};

/// Totaly ordered initial state - all spins DOWN (T=0).
/**
 * 
 * Spherical spin initialisatin class is expected to provide 
 * static function 'spin_init'
 * that receives 'constructor_arguments_number()' number of float arguments.
 * 
 * @param angle_type    type of angle-value
 * @param N             # of spin axes (dimensions);   N-1 == # of angles
 * 
 */
template<typename angle_type, size_t N>
struct HeisenbergSphericalInitDown{
    /// Number of random numbers that caller should provide to constructor
    /**
     * 
     * One angle is always zero --> there should be one rnd number less 
     * than in random init state.
     * 
     */
    static constexpr size_t constructor_arguments_number() { return N-1-1; }	

    typedef std::array<float, constructor_arguments_number()> Rnd_numbers;

    /// Array initialization 
    /**
     * 
     * @param rnd_numbers array of random numbers of float type (size N-2).
     *        Each element is used for initialisation of one spin angle
     * 		  and spin-angle[0] is initialised to 0.
     * @note  When spin-angle[0] is 0 projections on all but one axis are 0.
     * 
     */
    static std::array<angle_type, N-1> spin_init(const Rnd_numbers& rnd_numbers) {
		std::array<angle_type, N-1> angles;
		angles[0] = M_PI;
		std::transform( rnd_numbers.begin(), rnd_numbers.end(),
						angles.begin()+1,
						[](float rnd){
							return 2*M_PI * rnd;
						}
		);
        return angles;
    }
};


/// Defines bond energy for N3 spin
/**
 * 
 * @param spin_type  lattice point element type
 * @param N          # of spin axes (dimensions);   N=1 == # of angles
 * 
 */
template <typename spin_type, size_t N>
struct HeisenbergSphericalOperations3N {
        /// Bond energy
        static double bond_energy(const spin_type& l, const spin_type& r, float g) {
            using std::sin; using std::cos;
            return sin(l[1]) * cos(l[0]) * sin(r[1]) * cos(r[0])
                 + sin(l[1]) * sin(l[0]) * sin(r[1]) * sin(r[0])
                 + (1+g) * cos(l[1]) * cos(r[1]);
        }
        static std::array<double, N> magnetisation(const spin_type& l) {
            return {{ 
                    std::sin(l[1]) * std::cos(l[0]),   // Mx 
                    std::sin(l[1]) * std::sin(l[0]),   // My
                    std::cos(l[1]),                    // Mz
            }};
        }
        
        static constexpr size_t random_change_rnds_number() { return N-1; }	
        
        static spin_type & random_change(
                spin_type & s, 
                std::array<float, random_change_rnds_number()> rnds
        ) {
            for(size_t i=0; i<random_change_rnds_number(); ++i) {
                s[i] += rnds[i] * M_PI - M_PI/2;
            }
            return s;
        }
};

namespace magnetisation_impl {

    // Very important!!! 
    // Enables  C++ 'float cos(float)' overload
    // instead of C 'double cos(double)' & 'float cosf(float)'.
    using std::sin;
    using std::cos;
    
    /**
     * 
     * @param   N           dimensions number
     * @param   M           Magnetisations will be writen in the object of this type.
     *                      It is expected to provide operator[] for 0..N-1 elements.
     * @param   Angles      Type of object that provides angle-values (spin_type).
     *                      This object is expected to provide operator[] for 0..N-2 elements.
     */
    template<size_t N, typename M, typename Angles>
    class magnetisation_meta {

        template<size_t i, size_t f>
        static inline void mag_trig(M & m, const Angles & a, meta::int2type<true>) {
            m[i] *= cos(a[f]);
        }
        template<size_t i, size_t f>
        static inline void mag_trig(M & m, const Angles & a, meta::int2type<false>) {
            m[i] *= sin(a[f]);
            mag_trig<i, f+1>(m, a, meta::int2type<f+1==N-i-1>{});
        }

        template<size_t i>
        static inline void mag_meta_impl(M & m, const Angles & a, meta::int2type<true>) {
            ;
        }
        template<size_t i>
        static inline void mag_meta_impl(M & m, const Angles & a, meta::int2type<false>) {
            m[i] = 1;
            mag_trig<i, 0>(m, a, meta::int2type<N-i-1==0>{});
            mag_meta_impl<i+1>(m, a, meta::int2type<i+1==N>{});
        }

        template<size_t f>
        static inline double mag_01(const Angles & a, meta::int2type<true>) {
            return 1;
        }
        template<size_t f>
        static inline double mag_01(const Angles & a, meta::int2type<false>) {
            return sin(a[f]) * mag_01<f+1>(a, meta::int2type<N-2==f+1>{});
        }

      public:

        /**
         * 
         * @param m     array-like output object 0..N-1 to hold result
         * @param a     array-like input object 0..N-2 to hold result
         * 
         */
        static inline void magnetisation(M & m, const Angles & a) {
            static_assert(N>1, "Not defined for N<2!");
            double tmp = mag_01<0>(a, meta::int2type<N==2>{});
            m[0] = tmp * sin(a[N-2]);
            m[1] = tmp * cos(a[N-2]);
            mag_meta_impl<2>(m, a, meta::int2type<N-2==0>{});
        }
    };

} // namespace magnetisation_impl


/// Provides bond_energy(), magnetisation and random_change() for spin with N>1
/**
 * 
 * @param spin_type  lattice point element type
 * @param N          # of spin axes (dimensions);   N=1 == # of angles
 * 
 */
template <typename spin_type, size_t N>
struct HeisenbergSphericalOperations {
        /// Spin magnetisatin
        static std::array<double, N> magnetisation(const spin_type & l) {
            decltype( magnetisation(l) ) ret;
            magnetisation_impl::magnetisation_meta<N, decltype(ret), spin_type>::magnetisation(ret, l);
            return ret;
        }
        
        /// Bond energy
        static double bond_energy(const spin_type & l, const spin_type & r, float g) {
            double ret = 0;
            auto ml = magnetisation(l);
            auto mr = magnetisation(r);
            // All inner products but last are summed
            ret = std::inner_product(ml.begin(), ml.end()-1, mr.begin(), 0.);
            // Last product is multiplied by (1+g) and summed
            ret += (1+g) * ml[N-1] * mr[N-1];
            return ret;
        }
        
        /// Constant to multiply 'delta energy (de)' in Metropolis algorithm with.
        static constexpr float metropolis_coeficient() { return 0.5; }	

        /// Number of random numbers to be send to random_change().
        static constexpr size_t random_change_rnds_number() { return N-1; }	
        
        /// Randomly changes received spin
        /**
         * 
         * @param   s       Spin reference to be changed.
         * @param   rnds    Array of random numbers is used to implement spin change.
         * 
         */
        static void random_change(
                spin_type & s, 
                const std::array<float, random_change_rnds_number()> & rnds
        ) {
            // spin += rnd * M_PI - M_PI/2;
            std::transform(
                rnds.begin(), rnds.end(), 
                s.begin(),
                s.begin(),
                [](float rnd, typename spin_type::value_type spin_value) { 
                    return spin_value + rnd * M_PI - M_PI/2; 
                }
            );
        }
};


} // namespace

#endif
