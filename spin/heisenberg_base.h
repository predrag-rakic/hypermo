// Copyright 2013 Predrag Rakic
// predrag.rakic@gmail.com

#ifndef _HEISENBERG_BASE_H_
#define _HEISENBERG_BASE_H_

#include <ostream> //ostream
#include <array>
#include <cstdio>  // std::snprintf()
#include <limits>  // numeric_limits


/// Objects that live in lattice points
namespace spin {

/// Type of element that 'lives' on each lattice point
/**
 * @param N            		# of spin axes (dimensions);   N=1 == # of angles
 * @param value_type        type of spin state-representation values
 * @param SpinCoordSystem	class name - Spherical, Decartes, ... 
 * @param SpinInit     		class name - provides spin_init() function 
 * @param SpinOperations	class name - provides magnetisation() && bond_energy() functions
 * 
 * @note value_type is expected to provide:
 *  - value_type(T)      - constructor with one parameter (received from spin_init())
 *  - operator float() const - implicit conversion to float (or double)
 *  - value_type & operator += (float);
 *  - std::ostream & operator<<(std::ostream &, const value_type &)
 * 
 * @note SpinCoordSystem provides:
 *  - atributes
 *  - constructor with parameter received from SpinInit::spin_init()
 *  - operator[] to acess atributes
 * 
 * @note SpinInit is expected to provide:
 *  - default constructor
 *  - static function 'spin_init()' with number of float arguments (random numbers).
 *  - 'static constexpr size_t constructor_arguments_number()' - function
 *    that instructs caller how many arguments to provide to 'spin_init()'.

 * @note SpinOperations is expected to provide:
 *  - default constructor
 *  - static double bond_energy(const Spin::type &, const Spin::type &, float);
 *  - static std::array<double, N> magnetisation(const Spin::type &);
 *  - static constexpr size_t random_change_rnds_number();
 *  - static spin_type & random_change(spin_type & s, std::array<float, random_change_rnds_number()>);
 * 
 */ 
template <
        size_t N_,   
        typename value_t, 
        template<typename value_type, size_t N> class SpinCoordSystem,
        template<typename value_type, size_t N> class SpinInit,
        template<typename spin_type, size_t N> class SpinOperations
>
struct Heisenberg {
    //constexpr static size_t N = N_;
    typedef SpinCoordSystem<value_t, N_> coord_system_type;
    typedef SpinInit<value_t, N_> spin_init_type;
  public:

    struct type : 
                 coord_system_type, 
                 spin_init_type, 
                 SpinOperations<type, N_>
    {
        constexpr static size_t N = N_;
        typedef value_t value_type;
        typedef typename spin_init_type::Rnd_numbers Rnd_numbers;

        explicit type(const Rnd_numbers& rnds /**< Array of random numbers for spin initialisation */) 
            : coord_system_type(spin_init_type::spin_init(rnds)) 
        {}
        
        double bond_energy(const type& other, float g) const { 
            return SpinOperations<type, N>::bond_energy(*this, other, g); 
        }
        
        // Mz, My, Mx
        std::array<double, N> magnetisation() const { 
            return SpinOperations<type, N>::magnetisation(*this); 
        }
        
        /// Generic operator<<() - streams spin object
        friend std::ostream& operator<<(std::ostream& out,  const type & r) {
            auto precision = std::numeric_limits<value_type>::digits10;
            auto old_precision = out.precision(precision);
            std::operator<<(out, "{");
            using std::operator<<;
            out << (r[0]);
            for(auto i=1u; i<N-1; ++i) {
                std::operator<<(out, ", ");
                out << (r[i]);
            }
            std::operator<<(out, "}; ");
            out.precision(old_precision);
            return out;
        }

    };
};


} // namespace

#endif
