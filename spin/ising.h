// Copyright 2013 Predrag Rakic
// predrag.rakic@gmail.com

#ifndef _SPIN_ISING_H_
#define _SPIN_ISING_H_

#include <ostream> //ostream
#include <array>
#include <cmath>  // sin(), cos()

namespace spin {

/// Spin representation for Ising model.
/**
 * 
 * @param position_type     type of spin-position (can be up or down)
 * @param SpinInit     		class name - provides spin_init() function 
 * @param SpinOperations	class name - provides magnetisation() && bond_energy() functions
 * 
 */
template<
        typename position_type,         
        template<typename value_type> class SpinInit,
        template<typename spin_type> class SpinOperations
>
struct Ising  : 
            SpinInit<position_type>,
            SpinOperations<Ising<position_type, SpinInit, SpinOperations>>
{
    constexpr static size_t N = 1;
    typedef Ising<position_type, SpinInit, SpinOperations> ising_type;

    position_type value;

	explicit Ising(const typename SpinInit<position_type>::Rnd_numbers & rnd) 
		:	 value{ SpinInit<position_type>::spin_init(rnd) } 
	{}
    
    int bond_energy(const Ising & other) const { 
        return SpinOperations<ising_type>::bond_energy(*this, other); 
    }
    
    // Mx
    std::array<int, 1> magnetisation() const { 
        return SpinOperations<ising_type>::magnetisation(*this); 
    }

    friend std::ostream& operator<<(std::ostream& out, const Ising & spin) {
        std::operator<<(out, "{");
        using std::operator<<;
        out << (spin.value);
        std::operator<<(out, "}");
        return out;
    }
};


/// Totaly random initial state (T=inf).
/**
 * 
 * Randomly places spin positions up or down.
 * 
 * @param position_type type of angle-value
 * @param N             # of spin axes (dimensions);   N must be 1
 * 
 */
template<typename position_type>
struct IsingInitRandom {
	/// Number of random numbers that caller should provide to 'Spin' constructor.
	static constexpr size_t constructor_arguments_number() { 
        return 1; 
    }	

    typedef std::array<float, 1> Rnd_numbers;

    /// Spin initialisation
    /**
     * 
     * @param rnd_number array containing one random number of float type,
     *        used for initialisation of the spin position.
     * 
     */
    static position_type spin_init(const Rnd_numbers& rnd_number) {
        return static_cast<position_type>(rnd_number[0] > 0.5 ? 1 : -1) ;
    }
};


/// Totaly ordered initial state (T=0).
/**
 *
 * Places all spin positions up.
 *
 * @param position_type type of angle-value
 * @param N             # of spin axes (dimensions);   N must be 1
 *
 */
template<typename position_type>
struct IsingInitUp {
    /// Number of random numbers that caller should provide to 'Spin' constructor.
    static constexpr size_t constructor_arguments_number() {
        return 1;
    }

    typedef std::array<float, 1> Rnd_numbers;

    /// Spin initialisation
    /**
     *
     * @param rnd_number array containing one random number of float type,
     *        used for initialisation of the spin position.
     *
     */
    static position_type spin_init(const Rnd_numbers& rnd_number) {
        return static_cast<position_type>(1);
    }
};


/// Defines bond energy for N3 spin
/**
 * 
 * @param spin_type  lattice point element type
 * @param N          # of spin axes (dimensions);   N must be 1
 * 
 */
template <typename spin_type>
struct IsingOperations {
        /// Energy in bond between two spins
        static int bond_energy(const spin_type& l, const spin_type& r) {
            return l.value * r.value;
        }
        
        /// Spin magnetisation
        static std::array<int, 1> magnetisation(const spin_type & l) {
            return {{ l.value }};
        }

        /// Constant to multiply 'delta energy (de)' in Metropolis algorithm with.
        static constexpr float metropolis_coeficient() { return 1; }	
        
        /// Number of random numbers to be send to random_change().
        static constexpr size_t random_change_rnds_number() { return 0; }	

        static void random_change(
                spin_type & s, 
                std::array<float, random_change_rnds_number()> rnds
        ) {
            s.value *= -1;
        }
};


} // namespace

#endif
