#include <iostream>
#include <array>
#include "../RNG/ran1.h"
#include "../RNG/rng_sequence.h"
#include "sc.h"
#include "../lattice/operations.h"
#include "../spin/heisenberg_spherical.h"
#include "../spin/ising.h"

using std::endl;

namespace aaa{
    
struct AAA {
    char c;
    explicit AAA(float d) : c(d*30) { }
    operator float() const {return c; }
    AAA& operator+=(float that)  { c+=that; return *this; }
};
std::ostream& operator<<(std::ostream& os, const AAA & a) { os << static_cast<int>(a.c); return os; }

}

int main() {
    rng::RngSequence<rng::Ran1, 3> rngs(2,2,3);
    auto rrr = rngs();
    auto rngarr = rng::RngSequence<rng::Ran1, 3>::create(1);
    auto rrr2 = rngarr();

    
                         // N, spin_elem_type, init, Ebond_type
    typedef spin::Heisenberg<
                3, float, 
                spin::HeisenbergSpherical,			// Spherical coordinate system
                spin::HeisenbergSphericalInitRandom, 
                spin::HeisenbergSphericalOperations3N
            > Spin;

    typedef spin::Ising<
                short int, 
                spin::IsingInitRandom, 
                spin::IsingOperations
            > Ising;

                     // D, SpinInitRNG, idx_type
    typedef lattice::SimpleCubic<3, rng::Ran1>::type<Spin::type> Lattice;
    typedef lattice::SimpleCubic<3, rng::Ran1>::type<Ising> LatticeIsing;
    Lattice l({4u,4u,4u}, 1234);
    LatticeIsing ising{ {4u,4u,4u}, 12345};
    std::cout << "l.index({1u, 2u, 3u}) = " << l.index({1u, 2u, 3u}) << endl;
    std::cout << l({1u, 2u, 3u})[0] << endl;
    std::cout << l({1u, 2u, 3u}).bond_energy(l({1u, 2u, 1u}), 0) << endl;
    typename Spin::type aaa = l({1u,2u,3u});
    std::cout << "aaa = " << aaa << endl;
    aaa.magnetisation();
    l.print();
    std::cout << "Iteriram!" << endl;
    for(auto it=l.begin(); it!=l.end(); ++it) { 
        std::cout << *it; 
    }
    
    std::cout << endl;
    
    
    std::cout << endl;

    Lattice::iterator it = l.begin();
    std::cout << *it<< endl;
    std::cout << *(++it)<< endl;
    std::cout << it.index() << endl;
//    ++it;
    std::cout << "it.index() == " << it.index() << endl;
    typename decltype(l)::coordinate_type c{0u,0u,1u};
    std::cout << "c: " << c << endl;
    std::cout << "l.index(c) == " << l.index(c) << endl;
    auto ni = l.neighbor_indices(c);
    std::cout << "l.neighbor_indices(c)=" << ni[0] <<" "<< ni[1] <<" " << ni[2] <<" " << ni[3] <<" " << ni[4] <<" " << ni[5] << endl;
    for(auto i : ni) std::cout << i << " "; std::cout << endl;
//    Spin::type bb {1.1f,2.2f};
//    std::cout << bb<< endl;

    Lattice::coord_iterator cit = l.coord_begin();
    ++cit;
    auto coo = cit.coordinates();
    std::cout << "coordinates : " << coo << endl;
    std::cout << "cit.index() = " << cit.index() << endl;
    std::cout << "l.index(coordinates) = " << l.index(coo) << endl;
    if(cit.index() == cit.lattice().index(cit.coordinates())) std::cout << "isto\n"; else std::cout << "razlicito!\n";
    std::cout << *cit << endl;
    auto ll = cit.lattice();
    if(*cit == ll[cit.index()]) std::cout << "isto\n"; else std::cout << "razlicito!\n";
    for (auto cit = l.coord_begin(); cit!=l.coord_end(); ++cit) {
        std::cout << cit.index() << " " << cit.coordinates() << std::endl;
    }
//    l.set_g(0.002);
    auto spin_energy = lattice::SpinEnergy<decltype(l)>(l, 0.002);
    std::cout << "spin_energy(0u,0u,1u) = "<< spin_energy({0u,0u,1u}) << endl;
                               // Lattice, T,   seed
    auto therm = lattice::Thermalisation<rng::Ran1>::type<decltype(spin_energy)>(spin_energy, 1.1, 123);
    auto em = lattice::EnergyMagnetisation<decltype(spin_energy)>(spin_energy);
    std::cout << "e=" << em().energy << endl;
    std::cout << "l.magnetisation=" << em().magnetisation[0] <<" "<< em().magnetisation[1] <<" " << em().magnetisation[2] << endl;
    therm.sweep();
    std::cout << "spin_energy(0u,0u,1u) = "<< spin_energy({0u,0u,1u}) << endl;    
    std::cout << "e=" << em().energy << endl;
    std::cout << "l.magnetisation=" << em().magnetisation[0] <<" "<< em().magnetisation[1] <<" " << em().magnetisation[2] << endl;
    therm.sweep();
    std::cout << "e=" << em().energy << endl;
    std::cout << "l.magnetisation=" << em().magnetisation[0] <<" "<< em().magnetisation[1] <<" " << em().magnetisation[2] << endl;

//    l.at(123);
//    const_cast<const decltype(l) *>(&l) -> at(123);

    ising.print();   

    Lattice::coordinate_type c0 {0u,0u,0u};
    auto n = l.neighbor_indices<2>({0u,0u,0u});
    std::cout << "coordinate : " << c0 << "; neigbours<2> = ";
    for (auto i : n) std::cout << i << " ";
    std::cout << endl;
    
}
