// Copyright 2013 Predrag Rakic
// predrag.rakic@gmail.com

#ifndef _BCC_H_
#define _BCC_H_

//
// Simple cubic lattice
//

#include <iostream>
#include <string>
#include <sstream>
#include <array>
#include <vector>
#include <algorithm>
#include <functional>
#include <cassert>
#include <stdexcept>        // out_of_range
#include <type_traits>
#include "coordinate.h"
#include "../util/meta.h"
#include "../RNG/rng_sequence.h"


/// Lattice and lattice operations definitions
namespace lattice {


/// Body-centered cubic lattice
/** 
 * 
 * Basic point distance is a === 2 
 * (that way all allowed positions areintegral numbers).
 *
 * @param D                     number of lattice dimensions
 * @param SpinInitRNG           RNG type for spin initialization
 */ 
template<
    size_t D_, 
    typename SpinInitRNG
>
struct BodyCenteredCubic
{
  
    /**
     * @param spin_type  lattice point element type
     */
    template <
              typename spin_t
    >
    class type
    {
      public:
        static constexpr size_t D=D_;
        typedef unsigned axis_type;
        typedef spin_t spin_type;
        typedef Coordinate<axis_type, D> coordinate_type;

        const coordinate_type dims;
        const std::array<size_t, D> multipliers;
        const size_t lattice_size;
        const size_t size;

      private:
        // SC lattice points + another SC latice points - translated by 1/2 * (i,j,k,...)
        std::array<std::vector<spin_type>, 2> lattice;
        typedef rng::RngSequence<
            SpinInitRNG, 
            spin_type::constructor_arguments_number(), 
            float   // Spin constructor arguments are floats (not doubles).
        > spin_type_rngs_type;
        spin_type_rngs_type spin_type_rngs;

      public:

        /// Initialize lattice spins
        /**
         * Initialization policy is defined in spin_type class
         */
        void init() {
            lattice[0].clear();
            lattice[1].clear();
            lattice[0].reserve(lattice_size);
            lattice[1].reserve(lattice_size);
            for(unsigned i=0; i<lattice_size; ++i) {
                lattice[0].emplace_back(spin_type_rngs());
                lattice[1].emplace_back(spin_type_rngs());
            }
        }
        
        explicit type(const coordinate_type& d, long seed)
           :
                dims(d),
                // { 1, dims[0], dims[0]*dims[1], ..., dims[0]*...*dims[D-2] }
                multipliers(
                    [&d]() {
                        std::array<size_t, D> muls {{1}} ;
                        std::partial_sum(d.begin(), d.end()-1, 
                                         muls.begin()+1, 
                                         std::multiplies<size_t>{}
                        );
                        return muls;
                    } () // execute lambda
                ),
                lattice_size(std::accumulate(dims.begin(), dims.end(), 1ul,
                             std::multiplies<axis_type>())
                 ),
                size(2 * lattice_size),
                spin_type_rngs(spin_type_rngs_type::create(seed))
        {
            init();
        }
        
        enum spin_group { BASIC=0, TRANSLATED };
        typedef std::pair<spin_group, size_t> idx_tuple_type;

        spin_type& operator[](const idx_tuple_type & idx_array) {
            spin_group sg  = idx_array.first;
            size_t idx = idx_array.second;
            if(sg == BASIC) 
                return lattice[BASIC][idx]; 
            return lattice[TRANSLATED][idx]; 
        }
        const spin_type& operator[](const idx_tuple_type & idx_array) const {
            return const_cast<type &>(*this)[idx_array];
        }

        /// Computes index value from Coordinate object
        /**
         *
         * a==2 --> allowed positions are only those with:
         * - all axis values even ( spins in base latttice) {0, 2, 4, ... , (N-1)*2 == N*2-2
         * - all axis values odd ( spins in translated latttice) {1, 3, 5, ... , (N-1)*2+1 == N*2-1
         *
         * @note Argument is passed by value because copy is needed in the function body.
         *
        */
        idx_tuple_type index(coordinate_type coord) const {
            idx_tuple_type idx_array;
            // axis even -> 0 else 1
            idx_array.first  = ((coord[0] & 1) == 0) ? BASIC : TRANSLATED ;
            for(auto & i : coord) i/=2; 
            // my_index = coords[0]*multipliers[0] + coords[1]*multipliers[1] + ... + coords[D-1]*multipliers[D-1]
            idx_array.second = std::inner_product(
                            coord.begin(), coord.end(), 
                            multipliers.begin(), 
                            0ul
            );
            return idx_array;
        }

        /// Computes correct index value from of out-of-range Coordinate object
        /**
         * 
         * Normalise each coordinate axis value, 
         * but only if the value overflows no more than dims[i] in each dimension.
         *
         * @sa index()
         *
        */
        idx_tuple_type index_moduoN(coordinate_type coord) const {
            std::transform( 
                coord.begin(), coord.end(), 
                dims.begin(),
                coord.begin(),
                [](axis_type c, axis_type d) {
                    // negative overflow - unsigned representation of negative axis
                    if(c>(-1u-2*d))         return c+2*d;
                    // positive overflow
                    else if(c>=2*d)   return c-2*d;
                    // no overflow
                    return c;
                }
            );
            return index(coord);
        }

        spin_type& operator()(const coordinate_type& coord) {
            return operator[]( index(coord) );
        }
        const spin_type& operator()(const coordinate_type& coord) const {
            return operator[]( index(coord) );
        }

        /// Range-checking operator[]
        spin_type& at(const idx_tuple_type & idx_array) {
            // Call: "at() const"
            return const_cast<spin_type &>( const_cast<const type &>(*this).at(idx_array) );
        }
        /// Range-checking operator[]
        const spin_type& at(const idx_tuple_type & idx_array) const {
            if(idx_array.first!=BASIC && idx_array.first!=TRANSLATED)
                throw std::out_of_range(
                    "Lattice index_array.first==" + std::to_string(idx_array.first) + " is out of range!"
                );
            if(idx_array.second<0 || idx_array.second>=lattice_size)
                throw std::out_of_range(
                    "Lattice index_array.second==" + std::to_string(idx_array.second) + " is out of range!"
                );
            return operator[]( idx_array );
        }
        /// Range-checking operator()
        spin_type& at(const coordinate_type& coord) {
            // Call: "at() const"
            return const_cast<spin_type &>( const_cast<const type &>(*this).at(coord) );
        }
        /// Range-checking operator()
        const spin_type& at(const coordinate_type& coord) const {
            auto even_odd = (coord[0] & 1);
            for(auto & i : coord)
                if((i & 1) != even_odd) {
                    std::stringstream c;
                    c << coord;
                    throw std::out_of_range( "Nonexistent spin: " + c.str() );
            }
            auto idx_array = index(coord);
            return at(idx_array);
        }

        /// Print spin angles (debugging)
        void print(std::ostream& out=std::cout) const {
            for(auto & lat : lattice) {
                out << "\n[ ";
                for(auto & i : lat) {
                    out << i;
                }
                out << "] ";
            }
            out << std::endl;
        }

      private:
        
        /// Computes indices of the first neighbors
        /**
         * 
         * @param  coord    coordinate of designated spin
         * @return          array containing indices of neigbour spins
         * @note            First neighbors appear in D>=1 lattices.
         *                  There are 2^D first neigbours for each element 
         *                  in body-centered cubic lattice:
         *                  (x+1,y+1,z+1,...+1), (x-1,y+1,z+1,...+1), (x+1,y-1,z+1,...+1), (x-1,y-1,z+1,...+1), ...
         * 
        */
        std::array<idx_tuple_type, (1u<<D)> neighbor_indices(
                    const coordinate_type& coord,
                    meta::int2type<1>
        ) const {
            size_t permutations = (1u<<D);
            std::array<idx_tuple_type, (1u<<D)> neighbor_idxs;
            for(auto & neighbor_idx : neighbor_idxs) {
                size_t perm = --permutations;
                auto neigbour_coord = coord;
                for(auto & axis : neigbour_coord) {
                    if(perm & 1) ++axis;
                    else         --axis;
                    perm>>=1;
                }
                neighbor_idx = index_moduoN(neigbour_coord);
            }
            return neighbor_idxs;
        }

        /// Computes indices of the second neighbors
        /**
         * 
         * @param  coord    coordinate of designated spin
         * @return          array containing indices of neighbor spins
         * @note            Second neighbors appear in D>=2 lattices.
         *                  There are 2*D second neighbors for each element 
         *                  in body-centered cubic lattice:
         *                  (x+1,y,z,...), (x-1,y,z,...), (x,y+1,z,...), (x,y-1,z,...), ...
         * 
         */
        std::array<idx_tuple_type, 2*D> neighbor_indices(
                    const coordinate_type& coord,
                    meta::int2type<2>
        ) const {
            std::array<idx_tuple_type, 2*D> neighbor_idxs;
            // Appropriate type and value of 'i' initializer expresion
            // Something like  "i = 0u"
            for(auto i = decltype(coordinate_type::I){}; i<coordinate_type::I; ++i) {
                auto up = coord;
                up[i]   += 2;
                neighbor_idxs[2*i] = index_moduoN(up);
                auto down = coord;
                down[i] -= 2;
                neighbor_idxs[2*i+1] = index_moduoN(down);
            }
            return neighbor_idxs;
        }


      public:

        /// Computes indices of n-th neighbors
        /**
         * 
        * @param  coord     coordinate of designated spin (spin whose neighbor-indices are returned).
        * @return           array of size_t elems, containing indices of neigbour spins.
        * @note             Number of indices returned (size of array) depends on D and neighbous order.
        * 
         */
        template<size_t I>
        auto neighbor_indices(const coordinate_type& coord) const -> decltype(this->neighbor_indices(coord, meta::int2type<I>())) {
            return neighbor_indices(coord, meta::int2type<I>());
        }
        
        /// Defaults to 'neighbor_indices<1>(...)'
        /**
         * @sa neighbor_indices<>()
        */
        auto neighbor_indices(const coordinate_type& coord) const  -> decltype(this->neighbor_indices<1>(coord)) {
            return neighbor_indices<1>(coord);
        }



        /// Lattice::type iterator (default)
        /**
         * This iterator uses index to access (iterrate over) lattice spins.
         */
        class iterator {
          protected:
            type& l;
            spin_group tg{BASIC};   // target group
            size_t ti{0};           // target index;

          public:
            explicit iterator(type& l_, bool end_iterator = false) 
               : l(l_)
            {
                if(end_iterator) {
                    tg = static_cast<spin_group>(TRANSLATED+1);
//                    ti = l.lattice_size;
                }
            }
            
            type& lattice() { return l; }

            idx_tuple_type index() { 
                return {tg, ti}; 
            }
            spin_type& operator*() { 
                return l[ {tg, ti} ];
            }
            spin_type* operator->() { 
                return &l[ {tg, ti} ];
            }
            iterator& operator++() { 
                ++ti;
                if(ti == l.lattice_size) {
                    tg = static_cast<spin_group>(tg + 1);
                    ti = 0;
                }
                return *this; 
            }
            bool operator==(const iterator& that) { 
                return &l==&that.l && tg==that.tg && ti==that.ti;
            }
            bool operator!=(const iterator& that) { 
                return !(*this==that);
            }
        };

        /// Lattice::type iterator that 'knows' spin coordinates
        class coord_iterator : public iterator {
            coordinate_type tc;  // target coordinates;

          public:
            explicit coord_iterator(type& l_, bool end_iterator = false) 
               : iterator(l_, end_iterator), 
                 tc( std::array<unsigned, D>{} )
            { }
            
            coordinate_type coordinate() {
                return this->tc;
            }
            
            /// Point to the next spin element
            /**
             * 
             * Increment index and coordinate values.
             * 
             */
            coord_iterator& operator++() { 
                iterator::operator++();          // call base::operator++()
                unsigned axis_init_val = this->tg==BASIC ? 0 : 1;
                // start from least significant axis
                for(std::remove_cv<decltype(D)>::type d=0; d<D; ++d) {   
                    tc[d] += 2;                     // increment least sig. axis
                    if(tc[d] >= 2*this->l.dims[d])  // if axis equals axis limit
                        tc[d] = axis_init_val;          // axis <- 0
                    else                            // else 
                        break;                          // stop incrementing
                }
                return *this; 
            }
        };

        iterator begin() { return iterator(*this); }
        iterator end() { return iterator(*this, true); }
        coord_iterator coord_begin() { return coord_iterator(*this); }
        coord_iterator coord_end() { return coord_iterator(*this, true); }


        /// Generic operator<<() - streams idx_tuple_type object
        friend std::ostream& operator<<(std::ostream& out,  const idx_tuple_type & idx) {
            std::operator<<(out, "{");
            std::operator<<(out, std::get<0>(idx) == BASIC ? "BASIC" : "TRANSLATED");
            std::operator<<(out, ", ");
            out.operator<<(std::get<1>(idx));
            std::operator<<(out, "}; ");
            return out;
        }

    }; // class template type

}; // class template Lattice



} // namespace

#endif
