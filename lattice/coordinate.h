#ifndef _COORDINATE_H_
#define _COORDINATE_H_

#include <array>
#include <ostream>

namespace lattice {

/// Represents coordinates in n dimensional lattice
/**
 * Provides static guarantie that number of arguments is equal to the number of coordinates (dimensions).
 * 
 * @param axis_type     type of coordinate axis (array type)
 * @param I             number of dimensions
 */
template <typename axis_type, size_t I_>
struct Coordinate : public std::array<axis_type, I_> {
    static constexpr size_t I = I_;
    typedef std::array<axis_type, I> type;
    
    /// Captures std::array<> or cctor
    Coordinate(const type& co) : type(co) {}
    /// Captures initializer list, e.g. {1u,2u,3u}
    template <typename... Coord>
    Coordinate(Coord... co) : type({{co...}}) {
        static_assert(I==sizeof...(co), "Coordinate::Coordinate : Number of arguments not equal to number of dimensions");
    }
};

/// Generic operator<<() - streams Coordinate<> objects for any N and any axis_type.
/**
 * 
 * axis_type is epected to have oprator<<() defined.
 * 
 */
template <typename axis_type, size_t I>
std::ostream& operator<<(std::ostream& out, const Coordinate<axis_type, I>& coordinate) {
    std::operator<<(out, "{");
    using std::operator<<;
    out << (*(coordinate.begin()));
    for(auto i=coordinate.begin()+1; i!=coordinate.end(); ++i) {
        std::operator<<(out, ", ");
        out << (*i);
    }
    std::operator<<(out, "}");
    return out;
}

}

#endif
