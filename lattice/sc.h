// Copyright 2013 Predrag Rakic
// predrag.rakic@gmail.com

#ifndef _SC_H_
#define _SC_H_

//
// Simple cubic lattice
//

#include <iostream>
#include <string>
#include <array>
#include <vector>
#include <algorithm>
#include <cassert>
#include <stdexcept>        // out_of_range
#include <type_traits>
#include "coordinate.h"
#include "../util/meta.h"
#include "../RNG/rng_sequence.h"


/// Lattice and lattice operations definitions
namespace lattice {


/// Simple cubic lattice
/** 
 * @param D                     number of lattice dimensions
 * @param SpinInitRNG           RNG type for spin initialization
 */ 
template<size_t D_, 
        typename SpinInitRNG
>
struct SimpleCubic
{
  
    /**
     * @param spin_type  lattice point element type
     */
    template <
              typename spin_t
    >
    class type
    {
      public:
        static constexpr size_t D=D_;
        typedef unsigned axis_type;
        typedef spin_t spin_type;
        typedef Coordinate<axis_type, D> coordinate_type;

        const coordinate_type dims;
        const std::array<size_t, D> multipliers;
        const size_t lattice_size;

      private:
        std::vector<spin_type> lattice;
        typedef rng::RngSequence<
                        SpinInitRNG, 
                        spin_type::constructor_arguments_number(), 
                        float   // Spin constructor arguments are floats (not doubles).
        > spin_type_rngs_type;
        spin_type_rngs_type spin_type_rngs;

      public:

        /// Initialize lattice spins
        /**
         * Initialization policy is defined in spin_type class
         */
        void init() {
            lattice.clear();
            lattice.reserve(lattice_size);
            for(unsigned i=0; i<lattice_size; ++i) {
                lattice.emplace_back(spin_type_rngs());
            }
        }
        
        explicit type(const coordinate_type& d, long seed)
           :
                dims(d),
                // { 1, dims[0], dims[0]*dims[1], ..., dims[0]*...*dims[D-2] }
                multipliers(
                    [&d]() {
                        std::array<size_t, D> muls {{1}} ;
                        std::partial_sum(d.begin(), d.end()-1, 
                                         muls.begin()+1, 
                                         std::multiplies<size_t>{}
                        );
                        return muls;
                    } () // execute lambda
                ),
                lattice_size(std::accumulate(dims.begin(), dims.end(), 1ul,
                             std::multiplies<axis_type>())
                 ),
                spin_type_rngs(spin_type_rngs_type::create(seed))
        {
            init();
        }
        
              spin_type& operator[](const size_t & idx)       { return lattice[idx]; }
        const spin_type& operator[](const size_t & idx) const { return lattice[idx]; }
        
        /// Computes index value out of Coordinate object
        size_t index(const coordinate_type& coord) const {
            // my_index = coords[0]*multipliers[0] + coords[1]*multipliers[1] + ... + coords[D-1]*multipliers[D-1]
            return std::inner_product(
                coord.begin(), coord.end(), 
                multipliers.begin(), 
                0ul
            );
        }

        /// Computes correct index value out of out-of-range Coordinate object
        /**
         * 
         * Normalise each coordinate axis value, 
         * but only if the value overflows no more than dims[i] in each dimension.
         * @note calls 'index()'
         */
        size_t index_moduoN(const coordinate_type & coord) const {
            coordinate_type c{coord}; // Just becouse there is no default ctor.
            std::transform( 
                coord.begin(), coord.end(), 
                dims.begin(),
                c.begin(),
                [](axis_type c, axis_type d) {
                    // negative overflow - unsigned representation of negative axis
                    if(c>(-1u-d))         return c+d;
                    // positive overflow
                    else if(c>=d)   return c-d;
                    // no overflow
                    return c;
                }
            );
            return index(c);
        }

        spin_type& operator()(const coordinate_type& coord) {
            return operator[]( index(coord) );
        }
        const spin_type& operator()(const coordinate_type& coord) const {
            return operator[]( index(coord) );
        }

        /// Range-checking operator[]
        spin_type& at(const size_t & idx) {
            // Call: "at() const"
            return const_cast<spin_type &>( const_cast<const type &>(*this).at(idx) );
        }
        /// Range-checking operator[]
        const spin_type& at(const size_t & idx) const {
            if(idx<0 || idx>=lattice_size)
                throw std::out_of_range(
                    "Lattice index " + std::to_string(idx) + " is out of range!"
                );
            return operator[]( idx );
        }

        /// Print spin angles (debugging)
        void print(std::ostream& out=std::cout) const {
            for(auto & i : lattice) {
                out << i;
            }
            out << std::endl;
        }
        
      private:
        
        /// Computes indices of first neighbors
        /**
         * 
         * @param  coords   coordinate of designated spin
         * @return          array containing indices of neigbour spins
         * @note            First neighbors appear in D>=1 lattices.
         *                  There are 2*D first neigbours for each element 
         *                  in simple cubic lattice:
         *                  (x+1,y,z,...), (x-1,y,z,...), (x,y+1,z,...), (x,y-1,z,...), ...
         * 
         */
        std::array<size_t, 2*D> neighbor_indices(
                    const coordinate_type& coord,
                    meta::int2type<1>
        ) const {
            std::array<size_t, 2*D> neighbor_idxs;
            // Appropriate type and value of 'i' initializer expresion
            // Something like  "i = 0u"
            for(auto i = decltype(coordinate_type::I){}; i<coordinate_type::I; ++i) {
                auto up = coord;
                if(++up[i] == dims[i]) up[i] = 0;
                neighbor_idxs[2*i] = index(up);
                auto down = coord;
                if(--down[i] == -1u) down[i] = dims[i]-1;
                neighbor_idxs[2*i+1] = index(down);
            }
            return neighbor_idxs;
        }


        /// Computes indices of second neighbors
        /**
         * 
         * @param  coords   coordinate of designated spin
         * @return          array containing indices of neigbour spins
         * @note            Second neighbors appear in D>=2 lattices.
         *          There are 2*D*(D-1) second neighbors for each lattice element.
         *          Second neighbors are multiples of 4 (because 2 axes are mutated)
         *          D==2    # {(MUTx, MUTy)} == 4
         *          D==3    # {(MUTx, MUTy, z), (MUTx, y, MUTz), (z, MUTy, MUTz)} == 12
         *          D==4    # {(MUTx, MUTy, z, u), (MUTx, y, MUTz, u), (MUTx, y, z, MUTu), (x, MUTy, MUTz, u), ...} == 24
         *              
        */
        std::array<size_t, (2*D*(D-1))> neighbor_indices(
                    const coordinate_type& coord,
                    meta::int2type<2>
        ) const {
            std::array<size_t, (2*D*(D-1))> neighbor_idxs;
            auto idx_count=0u;
            for(auto i = 0u; i<D-1; ++i) {          // First coordinate mutation index
                for(auto j = i+1; j<D; ++j) {           // Second coordinate mutation index
                    for(auto k = 0; k<4; ++k) {             // 4 mutations: {++, +-, -+, --}
                        auto neigbour_coord = coord;
                        neigbour_coord[i] += (idx_count & 0b10u) != 0 ? 1 : -1;
                        neigbour_coord[j] += (idx_count & 1u) != 0 ? 1 : -1;
                        neighbor_idxs[idx_count++] = index_moduoN(neigbour_coord);
                    }
                }
            }
            return neighbor_idxs;
        }
        
      public:

        /// Computes indices of n-th neighbors
        /**
         * 
        * @param  coord     coordinate of designated spin (spin whose neighbor-indices are returned).
        * @return           array of size_t elems, containing indices of neigbour spins.
        * @note             Number of indices returned (size of array) depends on D and neighbous order.
        * 
         */
        template<size_t I>
        auto neighbor_indices(const coordinate_type& coord) const -> decltype(this->neighbor_indices(coord, meta::int2type<I>())) {
            return neighbor_indices(coord, meta::int2type<I>());
        }
        
        /// Defaults to 'neighbor_indices<1>(...)'
        auto neighbor_indices(const coordinate_type& coord) const  -> decltype(this->neighbor_indices<1>(coord)) {
            return neighbor_indices<1>(coord);
        }

        /// Lattice::type iterator (default)
        /**
         * This iterator uses index to access (iterrate over) lattice spins.
         */
        class iterator {
          protected:
            type& l;
            size_t ti;       // target index;

          public:
            explicit iterator(type& l_, bool end_iterator = false) 
               : l(l_), ti()
            {
                if(end_iterator) ti = l.lattice_size;
            }
            size_t index() { 
                return ti; 
            }
            spin_type& operator*() { 
                return l[ti];
            }
            spin_type* operator->() { 
                return &l[ti];
            }
            iterator& operator++() { 
                ++ti;
                return *this; 
            }
            bool operator==(const iterator& that) { 
                return &l==&that.l && ti==that.ti;
            }
            bool operator!=(const iterator& that) { 
                return !(*this==that);
            }
            type& lattice() { return l; }
        };

        /// Lattice::type iterator that 'knows' spin coordinates
        class coord_iterator : public iterator {
            coordinate_type tc;  // target coordinates;

          public:
            explicit coord_iterator(type& l_, bool end_iterator = false) 
               : iterator(l_, end_iterator), 
                 tc( std::array<unsigned, D>() )
            { }
            
            coordinate_type coordinates() {
                return this->tc;
            }
            
            /// Point to the next spin element
            /**
             * 
             * Increment index and coordinate values.
             * 
             */
            coord_iterator& operator++() { 
                iterator::operator++();          // call base::operator++()
                // start from least significant axis
                for(std::remove_cv<decltype(D)>::type d=0; d<D; ++d) {   
                    ++tc[d];                     // increment least sig. axis
                    if(tc[d]==this->l.dims[d])   // if axis equals axis limit
                        tc[d] = 0;                   // axis <- 0
                    else                         // else 
                        break;                       // stop incrementing
                }
                return *this; 
            }
        };

        iterator begin() { return iterator(*this); }
        iterator end() { return iterator(*this, true); }
        coord_iterator coord_begin() { return coord_iterator(*this); }
        coord_iterator coord_end() { return coord_iterator(*this, true); }
    }; // class template type

}; // class template Lattice



} // namespace

#endif
