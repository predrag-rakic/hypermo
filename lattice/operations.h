#ifndef _LATTICE_OPERATIONS_H_
#define _LATTICE_OPERATIONS_H_

#include <algorithm>
#include <cassert>
#include "coordinate.h"
#include "../RNG/rng_sequence.h"
#include "../util/logger.h"


namespace lattice {

/**
 * 
 * Compute energy for spin system composed of given-coordinate-spin
 * and its first neighbors.
 * 
 */
template<typename Lattice> 
struct NeighborsEnergy {
        typedef Lattice lattice_type;
        Lattice& l;
        NeighborsEnergy(Lattice& l_) : l(l_) 
        {}
        double operator()(const Coordinate<typename Lattice::axis_type, Lattice::D>& coord) {
            const auto& this_point = l(coord); 
            auto neigbors = l.neighbor_indices(coord);
            return -1 * std::accumulate(neigbors.begin(), neigbors.end(), 0.0,
                            [&](double sum, size_t idx) { 
                                return sum + this_point.bond_energy(l[idx]);
                            }
            );
        }
};

/**
 * 
 * Compute energy for spin system composed of given-coordinate-spin
 * and its first neighbors.
 * (single axis anisotropy - g parameter)
 * 
 */
template<typename Lattice> 
struct NeighborsEnergy_g {
        typedef Lattice lattice_type;
        Lattice& l;
        const float g;
        NeighborsEnergy_g(Lattice& l_, float g_) : l(l_), g(g_) {}
        double operator()(const Coordinate<typename Lattice::axis_type, Lattice::D>& coord) {
            const auto& this_point = l(coord); 
            auto neigbours = l.neighbor_indices(coord);
            return -1 * std::accumulate(neigbours.begin(), neigbours.end(), 0.0, 
                            [&](double sum, size_t idx) { 
                                return sum + this_point.bond_energy(l[idx], g);
                            }
            );
        }
};


/// Metropolis thermalisation algorithm.
/**
 * 
 * Metropolis algorithm is used to approach thermal equilibrium state .
 * 
 * @param RNG      random number generator type used for random spin changes 
 * 
 */
template<typename RNG>
struct Thermalisation {
    template<typename neighbors_energy_type>
    class type {
      public:
        neighbors_energy_type & neighbors_energy;
        typedef typename neighbors_energy_type::lattice_type lattice_type;
        lattice_type & l;
      private:
        const float T;
        RNG rngT;
        typedef rng::RngSequence<
                RNG, 
                lattice_type::spin_type::random_change_rnds_number(),
                float
        > rngs_type;
        rngs_type rngs;
      public:
      
        type(neighbors_energy_type& ne, float T_, long seed) 
            : 
                neighbors_energy(ne),
                l(ne.l),
                T(T_),
                rngT(seed),
                rngs(rngs_type::create(seed+1))
        {}

        /// Look for lower energy spin positions
        void lattice_sweep() {
            for(auto coord_it = l.coord_begin(); coord_it!=l.coord_end(); ++coord_it) {
                auto coor = coord_it.coordinates();
                double e  = neighbors_energy(coor); 
                auto orig = l(coor);
                lattice_type::spin_type::random_change(l(coor), rngs());
                double e1  = neighbors_energy(coor); 
                double de = e1 - e;   // delta energy
                de *= lattice_type::spin_type::metropolis_coeficient();
                if(de<0) { 
                    // keep new configuration configuration because it has lower energy
                    hy_log(10, " changed");
                } else if(rngT()<exp(-de/T)) {
                    // keep new configuration becouse of temperature
                    hy_log(10, " changed");
                } else {
                    // restore old configuration
                    l(coor) = orig;
                    hy_log(10, " same");
                }
            }
        }
        
        void sweeps(size_t sweeps_count=1) {
            for(size_t i=0; i<sweeps_count; ++i)
                lattice_sweep();
        }
    }; // class template type
}; // class template Thermalisation


template <typename neighbors_energy_type>
struct EnergyMagnetisation {
        neighbors_energy_type & neighbors_energy;
        typedef typename neighbors_energy_type::lattice_type lattice_type;
        lattice_type & l;
        constexpr static size_t N = lattice_type::spin_type::N;
        struct Output {
            double energy;
            std::array<double, N> magnetisation;
            Output() : energy(), magnetisation() {}
        };
        EnergyMagnetisation(neighbors_energy_type & ne) 
            :   neighbors_energy(ne),
                l(ne.l)
        {}
        Output operator()() {
            Output ret;
            for(auto coord_it = l.coord_begin(); coord_it!=l.coord_end(); ++coord_it) {
                ret.energy += neighbors_energy(coord_it.coordinates());
                auto m = coord_it->magnetisation();
                for(unsigned i=0; i<N; ++i)
                    ret.magnetisation[i] += m[i];
            }
            ret.energy /= 2 * l.lattice_size;
            for(unsigned i=0; i<N; ++i)
                ret.magnetisation[i] /= l.lattice_size;
            return ret; 
        }
};


}; // namespace

#endif
