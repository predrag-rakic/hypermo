#ifndef _RNG_ARRAY_H_
#define _RNG_ARRAY_H_

#include <vector>
#include <array>
#include "../util/meta.h"

namespace rng {
    /// Provides sequence of RNGs
    /**
     * @param RNG       type of RNGs in sequence
     * @param N         number of RNGs in sequence
     * @param ret_type  each random number in sequence is converted from its type 
     *                  (usually double) to ret_type
     */
    template<typename RNG, unsigned N, typename ret_type = double>
    class RngSequence {
        std::vector<RNG> rngs;
        typedef typename meta::make_indices_pack<N>::type Pack;
        
        template<size_t... Indices>
        std::array<ret_type,N> operator_braces_(
                        float scale, 
                        float shift, 
                        meta::indices_pack<Indices...>) 
        {
            return std::array<ret_type,N> {{ 
                static_cast<ret_type>( scale * rngs[Indices]() + shift )...  
            }};
        }
        template<size_t... Indices>
        static RngSequence create_(meta::indices_pack<Indices...>, long seed) {
            return RngSequence ( (seed + Indices)... ); 
        }
      public:

        /// Number of seeds must be equal to number of RNGs
        template<typename... Seeds>
        RngSequence(Seeds... seeds) : rngs({RNG(seeds)...}) {
            static_assert(sizeof...(Seeds) == N, 
                  "rng::RngSequence::RngSequence() : Number of seeds not equal to number of RNGs!");
        }
        
        /// operator() return value type
        typedef std::array<ret_type,N> type;
        
        /// Returns array of random numbers, each obtaind from coresponding RNG.
        /**
         * 
         * Each random value obtained from coresponding RNG is 
         * scaled and shifted: random_number * scale + shift
         * 
         * @param scale     
         * @param shift
         * 
         */
        type operator()(float scale = 1, float shift = 0) {
            return operator_braces_(scale, shift, Pack()); 
        }
        
        /// Creates RngSequence object
        /**
         * Each RNG object in the sequence is seeded with seed+index number.
         */
        static RngSequence create(long seed) {
            return create_(Pack(), seed); 
        }
    };
    
    

} // namespace

#endif
