#ifndef _RAN1_H_
#define _RAN1_H_

namespace rng {

//generator slucajnih brojeva iz [22] (Ran1.c)
#define IA 16807         
#define IM 2147483647    
#define AM (1.0/IM)      
#define IQ 127773        
#define IR 2836 
#define NTAB 32
#define NDIV (1+(IM-1)/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)

/// RNG class.
class Ran1 {
    long iy;
    long iv[NTAB];
    long idum;
    long neg_seed(long s) { return s<0 ? s : -s; }
  public:
    explicit Ran1(long seed_ = 0 /**< Seed value */) : 
        iy(0), idum(neg_seed(seed_)) 
    {}
    
    /// Set seed for RNG.
    /**
    All seed values are converted to negative values, so
    x and -x are the same seed.
    */
    void seed(long seed /**< New seed value */) { 
        idum = neg_seed(seed);  // Must be negative
    }
    
    /// Get next random number.
    /// @return value between 0 and 1
    double operator()() {
        int j;
        long k;
        double temp;
        if (idum <= 0 || !iy) {
            idum = -idum<1 ? 1 : -idum;
            for (j=NTAB+7;j>=0;j--) {
                k=(idum)/IQ;
                idum=IA*(idum-k*IQ)-IR*k;
                if (idum < 0) 
                    idum += IM;
                if (j < NTAB) 
                    iv[j] = idum;
            }   
            iy=iv[0];
        }
        k=(idum)/IQ;
        idum=IA*(idum-k*IQ)-IR*k;
        if(idum < 0) 
            idum += IM;
        j=iy/NDIV;
        iy=iv[j];
        iv[j] = idum;
        return (temp=AM*iy)>RNMX ? RNMX : temp;
    }
};

#undef IA 
#undef IM    
#undef AM
#undef IQ        
#undef IR 
#undef NTAB
#undef NDIV
#undef EPS 
#undef RNMX

} // namespace ran1

#endif
