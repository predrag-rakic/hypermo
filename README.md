# Hypermo

Hypermo is C++ template library for n dimensional spin system lattice simulations.

Library detals can be found in the [wiki](https://bitbucket.org/predrag-rakic/hypermo/wiki) section.
Quick introduction follows.

## Dependencies and instalation

- No real installation. 
- Just download, compile and run.
- No library dependancies.
- Requires gcc-4.7 or later.

### Download & compile (once):
```bash
git clone https://bitbucket.org/predrag-rakic/hypermo.git
cd hypermo/simulations
make
```

## Example simulations
There is a number of example simulations already assembled in the simulations directory:

- 3D-N3-SP    = Heisenberg model, 3 dimensional lattice, spin O(3) spherical coordinates, single-path simulation. 
- 3D-N3-MP    = Heisenberg model, 3 dimensional lattice, spin O(3) spherical coordinates, multipath simulation. 
- ising2D-SP  = Ising model, 2 dimensional lattice, single-path simulation. 

### Run (from the `simulations` directory):
```bash
make run EXE=... [parameters]
```
To run 3D Heisenberg single-path simulation with 3D lattice 
with 10 spins in each direction for temperature 1, 
with 3000 lattice sweeps for burn-in and 
another 20000 sweeps for simulation output, type:
```bash
make run EXE=3D-N3-SP L=10 T=10000 THERM=3000 SO=20000
```
This will produce simulation output file
`L10T10000THERM3000MC20000-xxx.dat`
in directory 
`~/mnt/store/sim_output/3D-N3-SP/g0/L10T10000/`.
Character `~` represents user's home directory. 
So if user name is 'jo', his home dir is usually `/home/jo/`.

The output file contains 5 columns:

  - the first one contains:
    - RNG seed in multipath simulation.
    - simulation output number in single-path simulation 
      (just to preserve the same number of columns).
  -  second column contains energy.
  - the rest (the last 3 columns) contain magnetizatin components (Mx, My and Mz).

# License

Hypermo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hypermo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hypermo.  If not, see <http://www.gnu.org/licenses/>.

