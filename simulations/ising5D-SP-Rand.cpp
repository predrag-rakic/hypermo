#include <hypermo/hypermo.h>
#include "getEnv.h"
#include "other.h"

int main() {
    // Numbr of points in each latice dimension
    auto L = getEnv<unsigned>("L", 0);

    // Temperature (should be divided by 10^4)
    double T = getEnv<double>("T", 0) / 10000.;  

    // Number of burn-in latice sweeps (termalisation steps - THERM)
    auto THERM = getEnv<unsigned long>("THERMALISATION");      

    // Number of simulation outputs
    unsigned SO = getEnv<unsigned long>("SO", 0);

    // Number of simulation outputs to drop (single path)
    unsigned DROP = getEnv<unsigned>("DROP");

    // g?
    float g = getEnv<float>("g");
    assert(g==0 && "Undefined for g<>0!");

    // Output file directory
    std::string dir = getEnv_string("DIR");

    // Output file name prefix
    std::string prefix = getEnv_string("PREFIX");
    
    // Output file name suffix
    std::string suffix = create_unique_file_name() + ".sp";
    
    // Output file name 
    std::string fname = dir + "/" + prefix + "-" + suffix;

    std::ofstream out(fname);

    typedef spin::Ising<
        short int, 
        spin::IsingInitRandom, 
        spin::IsingOperations
    > Spin;

    typedef lattice::SimpleCubic<
        5,              // 5D
        rng::Ran1       // Spin init RNG
    >::type<
        Spin
    > Lattice;


    auto seed_generator = SeedCESGA();
    
    auto seed = seed_generator.seed_get(0);
    Lattice l({L,L,L,L,L}, seed);
    auto neighbors_energy = lattice::NeighborsEnergy<decltype(l)>(l);
    
    typedef lattice::Thermalisation<rng::Ran1>::type<decltype(neighbors_energy)> Therm;
    auto thermalisation = Therm(neighbors_energy, T, seed);

    auto em = lattice::EnergyMagnetisation<decltype(neighbors_energy)>(neighbors_energy);
    
    thermalisation.sweeps(THERM);           // burn-in
    auto sim_output = em();                 // 1st SO
    util::sink(out, 0, sim_output);
    for(auto so=1u; so<SO; ++so) {          // the rest SOs
        thermalisation.sweeps(DROP+1);      // Number of states to DROP
        auto sim_output = em();
        // Just _one_ seed -> some mutualy different values required on that position!
        util::sink(out, so, sim_output);
    }
}
