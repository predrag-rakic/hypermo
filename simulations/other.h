#ifndef _OTHER_H_
#define _OTHER_H_

#include<string>
#include<cstring>
#include<limits.h>
#include<unistd.h>
#include<errno.h>
#include<stdexcept>
#include<sstream>


/// Generate unique file name concatinating host name with pid.
std::string create_unique_file_name() {
    char hostName[HOST_NAME_MAX];
    if(gethostname(hostName, HOST_NAME_MAX) != 0)
        throw std::runtime_error(
              "gethostname returned: " + std::string(strerror(errno))
        );
    
    pid_t pid = getpid();
    std::stringstream ss;
    ss << hostName << pid;
    
    std::string fname;
    getline(ss, fname);
    
    return fname;
}



/// Generates RNG seeds - implementation appropriate for CESGA.
struct SeedCESGA {

    ///
    /**
    Multipath simulation is conducted in number of simulation steps.
    Each simulation step independantly executes simulation algorithm and
    needs unique RNG seed.
    @return RNG seed.
    */
    long seed_get(unsigned index /**< identifes particular simulation step.*/
    ) const {
        static const long hostnameHash =
                std::hash<std::string>()(create_unique_file_name());
        long seed = hostnameHash + index;
        // Ovo je potrebno za Slobodanov ran1 RNG.
        return (seed<0 ? -seed : seed) ;
    }
};


#endif
