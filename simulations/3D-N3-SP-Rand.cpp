#include <hypermo/hypermo.h>
#include "getEnv.h"
#include "other.h"

int main() {
    // Numbr of points in each latice dimension
    auto L = getEnv<unsigned>("L", 0);

    // Temperature (should be divided by 10^4)
    double T = getEnv<double>("T", 0) / 10000.;  

    // Number of burn-in latice sweeps (termalisation steps - THERM)
    auto THERM = getEnv<unsigned long>("THERMALISATION", 0);      

    // Number of simulation outputs
    unsigned SO = getEnv<unsigned long>("SO", 0);

    // Number of simulation outputs to drop (single path)
    unsigned DROP = getEnv<unsigned>("DROP");

    // g?
    float g = getEnv<float>("g");

    // Output file directory
    std::string dir = getEnv_string("DIR");

    // Output file name prefix
    std::string prefix = getEnv_string("PREFIX");
    
    // Output file name suffix
    std::string suffix = create_unique_file_name() + ".sp";
    
    // Output file name 
    std::string fname = dir + "/" + prefix + "-" + suffix;

    std::ofstream out(fname);

    typedef spin::Heisenberg<
        3,                                  // 3N 
        float,                              // spin angle type
        spin::HeisenbergSpherical,          // Spherical coordinate system
        spin::HeisenbergSphericalInitRandom, 
        spin::HeisenbergSphericalOperations 
    > Spin;

    typedef lattice::SimpleCubic<
        3,              // 3D
        rng::Ran1       // Spin init RNG
    >::type<
        Spin::type
    > Lattice;


    auto seed_generator = SeedCESGA();
    
    auto seed = seed_generator.seed_get(0);
    Lattice l({L,L,L}, seed);
    auto se = lattice::NeighborsEnergy_g<decltype(l)>(l, g);
    auto thermalisation = lattice::Thermalisation<rng::Ran1>::type<decltype(se)>(se, T, seed);
    auto em = lattice::EnergyMagnetisation<decltype(se)>(se);
    
    
    thermalisation.sweeps(THERM);            // burn-in
    auto sim_output = em();                 // 1st SO
    util::sink(out, 0, sim_output);
    for(auto so=1u; so<SO; ++so) {          // the rest SOs
        thermalisation.sweeps(DROP+1);           // Number of states to DROP
        auto sim_output = em();
        // Just _one_ seed -> some mutualy different values required on that position!
        util::sink(out, so, sim_output);
    }
}
