#ifndef _GETENV_H_
#define _GETENV_H_

#include<iostream>
#include<sstream>
#include<string>
#include<cstdlib>
#include<stdexcept>


/// Get environment variable value as string
/**
@return Environment variable value as string
@throw std::runtime_error  if variable name does not exist in environment.
*/
std::string getEnv_string(const std::string& name /**< Environment variable name */) {
    char * cval = std::getenv(name.c_str());
    if(cval==NULL) {
        throw std::runtime_error(
              "Environment variable " + name + " is not defined!\n\n"
        );
    }
    return cval;
}

/// Get environment variable value and return it as type T
/**
@return Environment variable value of type T
@note Calls getEnv_string() function
*/
template<typename T>
T getEnv(const std::string& name /**< Environment variable name */) {
    std::stringstream ss;
    ss << getEnv_string(name);
    T value;
    ss >> value;
    return value;
}

/// Get environment variable value and return it as type T
/**
@return Environment variable value of type T (type of forbiddenValue)
@note Calls getEnv_string() function
@throw std::runtime_error if variable value equals forbiddenValue. 
*/
template<typename T>
T getEnv(const std::string& name /**< Environment variable name */, 
         const T forbiddenValue /**< Value that environment variable 
                                     must not have */) {
    T value = getEnv<T>(name);
    if(value==forbiddenValue) {
        std::stringstream ss;
        ss << name << " value cannot be " << forbiddenValue << "!!!\n\n";
        std::string error;
        std::getline(ss, error);
        throw std::runtime_error(error);
    }
    return value;
}


#endif
