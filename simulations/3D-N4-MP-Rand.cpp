#include <hypermo/hypermo.h>
#include "getEnv.h"
#include "other.h"

int main() {
    // Numbr of points in each latice dimension
    auto L = getEnv<unsigned>("L", 0);

    // Temperature (should be divided by 10^4)
    double T = getEnv<double>("T", 0) / 10000.;  

    // Number of burn-in latice sweeps (termalisation steps - THERM)
    auto THERM = getEnv<unsigned>("THERMALISATION", 0);      

    // Number of simulation paths - # of simulation outputs
    unsigned SO = getEnv<unsigned>("SO", 0);

    // g?
    float g = getEnv<float>("g");

    // Output file directory
    std::string dir = getEnv_string("DIR");

    // Output file name prefix
    std::string prefix = getEnv_string("PREFIX");
    
    // Output file name suffix
    std::string suffix = create_unique_file_name() + ".mp";
    
    // Output file name 
    std::string fname = dir + "/" + prefix + "-" + suffix;

    std::ofstream out(fname);

    typedef spin::Heisenberg<
        4,                                  // 4N 
        float,                              // spin angle type
        spin::HeisenbergSpherical,	        // Spherical coordinate system
        spin::HeisenbergSphericalInitRandom, 
        spin::HeisenbergSphericalOperations
    > Spin;

    typedef lattice::SimpleCubic<
        3,              // 3D
        rng::Ran1       // Spin init RNG
    >::type<
        Spin::type
    > Lattice;


    auto seed_generator = SeedCESGA();
    
    for(auto sp=SO; sp>0; --sp) {   // # simulation paths = # of sim. outputs
        auto seed = seed_generator.seed_get(sp);
        Lattice l({L,L,L}, seed);
        auto se = lattice::NeighborsEnergy_g<decltype(l)>(l, g);
        auto thermalisation = lattice::Thermalisation<rng::Ran1>::type<decltype(se)>(se, T, seed);
        auto em = lattice::EnergyMagnetisation<decltype(se)>(se);
        
        thermalisation.sweeps(THERM);
        auto sim_output = em();
        util::sink(out, seed, sim_output);
    }
}
