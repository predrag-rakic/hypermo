#include <unittest++/UnitTest++.h>

#include <array>
#include <vector>
#include <set>
#include <algorithm>
#include <numeric>
#include <stdexcept>        // out_of_range
#include <ostream>          // cout
#include <string>         
#include "../RNG/ran1.h"
#include "../RNG/rng_sequence.h"
#include "../lattice/bcc.h"
#include "../spin/heisenberg_spherical.h"

typedef spin::Heisenberg<
            3, float, 
            spin::HeisenbergSpherical,			// Spherical coordinate system
            spin::HeisenbergSphericalInitRandom, 
            spin::HeisenbergSphericalOperations3N
        > Spin;

typedef lattice::BodyCenteredCubic<3, rng::Ran1>::type<Spin::type> Latticex3;
typedef lattice::BodyCenteredCubic<2, rng::Ran1>::type<Spin::type> Latticex2;
typedef lattice::BodyCenteredCubic<1, rng::Ran1>::type<Spin::type> Latticex1;

struct LatticeFixture {
    Latticex3 l456{{4u,5u,6u}, 1234};
    Latticex2 l45{{4u,5u}, 1234};
    Latticex1 l4{{4u}, 1234};
};

////////////////////////
template<typename T>
std::ostream & operator<<(std::ostream & out, const std::set<T> & s) {
    for(auto & i : s) out << i << ",";
    return out;
}
template<typename T>
std::ostream & operator<<(std::ostream & out, const std::vector<T> & s) {
    for(auto & i : s) out << i << ",";
    return out;
}

TEST_FIXTURE(LatticeFixture, bcc_size_1) {
    CHECK_EQUAL(4*5*6, l456.lattice_size );
    CHECK_EQUAL(4*5*6 * 2, l456.size );
}

TEST_FIXTURE(LatticeFixture, bcc_operator_index_1) {
    auto v = Spin::type{ {{0.12345, 0.54321}} };
    Latticex3::idx_tuple_type idx_array {Latticex3::BASIC, 0} ;
    l456[ idx_array ] = v;
    const auto & cl456 = l456;
    CHECK_EQUAL(v, cl456[ idx_array ] );
}
TEST_FIXTURE(LatticeFixture, bcc_operator_index_2) {
    auto v = Spin::type{ {{0.12345, 0.54321}} };
    Latticex3::idx_tuple_type idx_array {Latticex3::TRANSLATED, 0} ;
    l456[ idx_array ] = v;
    const auto & cl456 = l456;
    CHECK_EQUAL(v, cl456[ idx_array ] );
}

TEST_FIXTURE(LatticeFixture, bcc_lattice_index_1) {
    Latticex3::coordinate_type c{0u,0u,0u};
    Latticex3::idx_tuple_type idx_array {Latticex3::BASIC, 0};
    CHECK_EQUAL( idx_array.first, l456.index(c).first );
    CHECK_EQUAL( idx_array.second, l456.index(c).second );
}
TEST_FIXTURE(LatticeFixture, bcc_lattice_index_2) {
    Latticex3::coordinate_type c{2u,2u,2u};
    Latticex3::idx_tuple_type idx_array {Latticex3::BASIC, 2/2+2/2*4+2/2*4*5};
    CHECK_EQUAL( idx_array.first, l456.index(c).first );
    CHECK_EQUAL( idx_array.second, l456.index(c).second );
}
TEST_FIXTURE(LatticeFixture, bcc_lattice_index_3) {
    Latticex3::coordinate_type c{6u,8u,10u};
    Latticex3::idx_tuple_type idx_array {Latticex3::BASIC, 6/2+8/2*4+10/2*4*5};
    CHECK_EQUAL( idx_array.first, l456.index(c).first );
    CHECK_EQUAL( idx_array.second, l456.index(c).second );
}
TEST_FIXTURE(LatticeFixture, bcc_lattice_index_4) {
    Latticex3::coordinate_type c{1u,1u,1u};
    Latticex3::idx_tuple_type idx_array {Latticex3::TRANSLATED, 1/2+(1/2)*4+(1/2)*4*5};
    CHECK_EQUAL( idx_array.first, l456.index(c).first );
    CHECK_EQUAL( idx_array.second, l456.index(c).second );
}
TEST_FIXTURE(LatticeFixture, bcc_lattice_index_5) {
    Latticex3::coordinate_type c{1u,3u,5u};
    Latticex3::idx_tuple_type idx_array {Latticex3::TRANSLATED, 1/2+(3/2)*4+(5/2)*4*5};
    CHECK_EQUAL( idx_array.first, l456.index(c).first );
    CHECK_EQUAL( idx_array.second, l456.index(c).second );
}
TEST_FIXTURE(LatticeFixture, bcc_lattice_index_6) {
    Latticex3::coordinate_type c{7u,9u,11u};
    Latticex3::idx_tuple_type idx_array {Latticex3::TRANSLATED, 7/2+(9/2)*4+(11/2)*4*5};
    CHECK_EQUAL( idx_array.first, l456.index(c).first );
    CHECK_EQUAL( idx_array.second, l456.index(c).second );
}

TEST_FIXTURE(LatticeFixture, bcc_lattice_index_moduoN_1) {
    CHECK_EQUAL((2*4-1u)/2, std::get<1>(l4.index_moduoN({-1u})) );
}
TEST_FIXTURE(LatticeFixture, bcc_lattice_index_moduoN_2) {
    CHECK_EQUAL((2*4-8u)/2, std::get<1>(l4.index_moduoN({8u})) );
}
TEST_FIXTURE(LatticeFixture, bcc_lattice_index_moduoN_3) {
    CHECK_EQUAL((2*4-1u)/2+(2*5-1u)/2*4, std::get<1>(l45.index_moduoN({-1u, -1u})) );
}
TEST_FIXTURE(LatticeFixture, bcc_lattice_index_moduoN_4) {
    CHECK_EQUAL((2*4-1u)/2+(2*5-1u)/2*4+(2*6-1u)/2*4*5, std::get<1>(l456.index_moduoN({-1u, -1u, -1u})) );
}
TEST_FIXTURE(LatticeFixture, bcc_lattice_index_moduoN_5) {
    CHECK_EQUAL((2*4-8u)/2+(2*5-10u)/2*4+(2*6-12u)/2*4*5, std::get<1>(l456.index_moduoN({8u, 10u, 12u})) );
}
TEST_FIXTURE(LatticeFixture, bcc_lattice_index_moduoN_6) {
    CHECK_EQUAL((2*4-8u)/2+(2*5-10u)/2*4+(2*6-12u)/2*4*5, std::get<1>(l456.index_moduoN({9u, 11u, 13u})) );
}
TEST_FIXTURE(LatticeFixture, index_moduoN_1) {
    CHECK_EQUAL(l456.index({0u,0u,0u}), l456.index_moduoN({8u,0u,0u}) );
}

TEST_FIXTURE(LatticeFixture, bcc_operator_brackets_1) {
    auto v = Spin::type{ {{0.12345, 0.54321}} };
    Latticex3::coordinate_type c{0u,0u,0u};
    Latticex3::idx_tuple_type idx_array {Latticex3::BASIC, 0+0*4+0*4*5};
    l456[idx_array] = v;
    CHECK_EQUAL(v, l456(c));
}
TEST_FIXTURE(LatticeFixture, bcc_operator_brackets_2) {
    auto v = Spin::type{ {{0.12345, 0.54321}} };
    Latticex3::coordinate_type c{1u,3u,5u};
    Latticex3::idx_tuple_type idx_array {Latticex3::TRANSLATED, 1/2+3/2*4+5/2*4*5};
    l456[idx_array] = v;
    CHECK_EQUAL(v, l456(c));
}

TEST_FIXTURE(LatticeFixture, bcc_at_index_1) {
    Latticex3::idx_tuple_type idx_array {static_cast<Latticex3::spin_group>(2), 1+3*4+5*4*5};
    CHECK_THROW(l456.at(idx_array), std::out_of_range);
}
TEST_FIXTURE(LatticeFixture, bcc_at_index_2) {
    Latticex3::idx_tuple_type idx_array {Latticex3::BASIC, -1};
    CHECK_THROW(l456.at(idx_array), std::out_of_range);
}
TEST_FIXTURE(LatticeFixture, bcc_at_index_3) {
    Latticex3::idx_tuple_type idx_array {Latticex3::BASIC, 4*5*6};
    CHECK_THROW(l456.at(idx_array), std::out_of_range);
}
TEST_FIXTURE(LatticeFixture, bcc_at_index_4) {
    auto v = Spin::type{ {{0.12345, 0.54321}} };
    Latticex3::idx_tuple_type idx_array {Latticex3::BASIC, 0} ;
    l456.at( idx_array ) = v;
    const auto & cl456 = l456;
    CHECK_EQUAL(v, cl456.at( idx_array ) );
}
TEST_FIXTURE(LatticeFixture, bcc_at_index_5) {
    auto v = Spin::type{ {{0.12345, 0.54321}} };
    Latticex3::idx_tuple_type idx_array {Latticex3::TRANSLATED, 0} ;
    l456.at( idx_array ) = v;
    const auto & cl456 = l456;
    CHECK_EQUAL(v, cl456.at( idx_array ) );
}
TEST_FIXTURE(LatticeFixture, bcc_at_coord_1) {
    auto v = Spin::type{ {{0.12345, 0.54321}} };
    Latticex3::coordinate_type c{0u,0u,0u};
    Latticex3::idx_tuple_type idx_array = l456.index(c);
    l456.at( idx_array ) = v;
    CHECK_EQUAL(v, l456.at(c) );
}
TEST_FIXTURE(LatticeFixture, bcc_at_coord_2) {
    auto v = Spin::type{ {{0.12345, 0.54321}} };
    Latticex3::coordinate_type c{6u,8u,10u};
    Latticex3::idx_tuple_type idx_array = l456.index(c);
    l456.at( idx_array ) = v;
    CHECK_EQUAL(v, l456.at(c) );
}
TEST_FIXTURE(LatticeFixture, bcc_at_coord_3) {
    auto v = Spin::type{ {{0.12345, 0.54321}} };    
    Latticex3::coordinate_type c{7u,9u,11u};
    Latticex3::idx_tuple_type idx_array = l456.index(c);
    l456.at( idx_array ) = v;
    const auto & cl456 = l456;
    CHECK_EQUAL(v, cl456.at(c) );
}
// overflaw
TEST_FIXTURE(LatticeFixture, bcc_at_coord_4) {
    Latticex3::coordinate_type c{8u,8u,10u};
    CHECK_THROW(l456.at(c), std::out_of_range);
    Latticex3::coordinate_type c2{9u,9u,11u};
    CHECK_THROW(l456.at(c2), std::out_of_range);
    Latticex3::coordinate_type c3{6u,8u,12u};
    CHECK_THROW(l456.at(c3), std::out_of_range);
}
// nonexistent
TEST_FIXTURE(LatticeFixture, bcc_at_coord_5) {
    Latticex3::coordinate_type c{0u,2u,5u};
    CHECK_THROW(l456.at(c), std::out_of_range);
    Latticex3::coordinate_type c2{1u,3u,4u};
    CHECK_THROW(l456.at(c2), std::out_of_range);
    Latticex3::coordinate_type c3{0u,3u,5u};
    CHECK_THROW(l456.at(c3), std::out_of_range);
}

TEST(bcc_print_1) {
    Latticex3 l{{1u,1u,1u}, 1234};
    auto v = Spin::type{ {{0.12345, 0.54321}} };
    std::stringstream spin_stream;
    spin_stream << v;
    l.at({0u,0u,0u}) = v;
    l.at({1u,1u,1u}) = v;
    std::stringstream lat_stream;
    l.print(lat_stream);
    CHECK_EQUAL("\n[ " + spin_stream.str() + "] \n[ " + spin_stream.str() + "] \n", lat_stream.str() );
}

TEST_FIXTURE(LatticeFixture, bcc_neighbor_indices_1___1) {
    for(auto coord_it = l456.coord_begin(); coord_it!=l456.end(); ++coord_it) {
        for(auto i : l456.neighbor_indices(coord_it.coordinate())) {
            CHECK_EQUAL(l456[i], l456.at(i)); 
        }
    }
}
TEST_FIXTURE(LatticeFixture, bcc_neighbor_indices_1___2) {
    auto ni_ = l4.neighbor_indices<1>({2u});
    std::set<Latticex1::idx_tuple_type> ni(ni_.begin(), ni_.end());
    std::set<Latticex1::idx_tuple_type> expected {{Latticex1::TRANSLATED, 0}, {Latticex1::TRANSLATED, 1}};
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, bcc_neighbor_indices_1___2a1) {
    auto ni_ = l4.neighbor_indices<1>({0u});
    std::set<Latticex1::idx_tuple_type> ni(ni_.begin(), ni_.end());
    std::set<Latticex1::idx_tuple_type> expected {{Latticex1::TRANSLATED, 0}, {Latticex1::TRANSLATED, 3}};
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, bcc_neighbor_indices_1___2b) {
    auto ni_ = l4.neighbor_indices<1>({1u});
    std::set<Latticex1::idx_tuple_type> ni(ni_.begin(), ni_.end());
    std::set<Latticex1::idx_tuple_type> expected {{Latticex1::BASIC, 0}, {Latticex1::BASIC, 1}};
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, bcc_neighbor_indices_1___3) {
    auto ni_ = l45.neighbor_indices<1>({2u,2u});
    std::set<Latticex2::idx_tuple_type> ni(ni_.begin(), ni_.end());
    std::set<Latticex2::idx_tuple_type> expected {
           {Latticex2::TRANSLATED, 5}, {Latticex2::TRANSLATED, 1},
           {Latticex2::TRANSLATED, 4}, {Latticex2::TRANSLATED, 0}
    };
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, bcc_neighbor_indices_1___4) {
    auto ni_ = l456.neighbor_indices<1>({0u,0u,0u});
    std::set<Latticex3::idx_tuple_type> ni(ni_.begin(), ni_.end());
    std::set<Latticex3::idx_tuple_type> expected {
           {Latticex3::TRANSLATED, 0}, {Latticex3::TRANSLATED, 1},
           {Latticex3::TRANSLATED, 4}, {Latticex3::TRANSLATED, 0}
    };
}

TEST_FIXTURE(LatticeFixture, bcc_neighbor_indices_2___1) {
    auto ni_ = l45.neighbor_indices<2>({2u,2u});
    std::set<Latticex2::idx_tuple_type> ni(ni_.begin(), ni_.end());
    std::set<Latticex2::idx_tuple_type> expected {
        l45.index({0u,2u}), l45.index({4u,2u}), l45.index({2u,0u}), l45.index({2u, 4u})
    };
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, bcc_neighbor_indices_2___2) {
    auto ni_ = l45.neighbor_indices<2>({3u,3u});
    std::set<Latticex2::idx_tuple_type> ni(ni_.begin(), ni_.end());
    std::set<Latticex2::idx_tuple_type> expected {
        l45.index({1u,3u}), l45.index({5u,3u}), l45.index({3u,1u}), l45.index({3u, 5u})
    };
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, bcc_neighbor_indices_2___3) {
    auto ni_ = l45.neighbor_indices<2>({0u,0u});
    std::set<Latticex2::idx_tuple_type> ni(ni_.begin(), ni_.end());
    std::set<Latticex2::idx_tuple_type> expected {
        l45.index({8-2u,0u}), l45.index({2u,0u}), l45.index({0u,10-2u}), l45.index({0u, 2u})
    };
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, bcc_neighbor_indices_2___4) {
    auto ni_ = l45.neighbor_indices<2>({1u,1u});
    std::set<Latticex2::idx_tuple_type> ni(ni_.begin(), ni_.end());
    std::set<Latticex2::idx_tuple_type> expected {
        l45.index({8-1u,1u}), l45.index({3u,1u}), l45.index({1u,10-1u}), l45.index({1u, 3u})
    };
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, bcc_neighbor_indices_2___5) {
    auto ni_ = l45.neighbor_indices<2>({6u,8u});
    std::set<Latticex2::idx_tuple_type> ni(ni_.begin(), ni_.end());
    std::set<Latticex2::idx_tuple_type> expected {
        l45.index({4u,8u}), l45.index({0u,8u}), l45.index({6u,6u}), l45.index({6u, 0u})
    };
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, bcc_neighbor_indices_2___10) {
    auto & l = l456;
    auto ni_ = l.neighbor_indices<2>({2u,2u,2u});
    std::set<decltype(ni_)::value_type> ni(ni_.begin(), ni_.end());
    std::set<decltype(ni_)::value_type> expected {
        l.index({0u,2u,2u}), l.index({4u,2u,2u}), l.index({2u,0u,2u}), 
        l.index({2u,4u,2u}), l.index({2u,2u,0u}), l.index({2u,2u,4u})
    };
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, bcc_neighbor_indices_2___11) {
    auto & l = l456;
    auto ni_ = l.neighbor_indices<2>({0u,0u,0u});
    std::set<decltype(ni_)::value_type> ni(ni_.begin(), ni_.end());
    std::set<decltype(ni_)::value_type> expected {
        l.index({8-2u,0u,0u}), l.index({2u,0u,0u}), l.index({0u,10-2u,0u}), 
        l.index({0u,2u,0u}), l.index({0u,0u,12-2u}), l.index({0u,0u,2u})
    };
    CHECK_EQUAL(expected, ni); 
}

TEST_FIXTURE(LatticeFixture, bcc_iterator_1) {
    unsigned count{0};
    auto v = Spin::type{ {{0.12345, 0.54321}} };
    for(auto & i : l456) {
        ++count;
        i = v;
    }
    CHECK_EQUAL(l456.size, count);

    for(auto & i : l456) {
        CHECK_EQUAL(v, i);
    }
}
TEST_FIXTURE(LatticeFixture, bcc_iterator_2) {
    CHECK_EQUAL(&l456, &(l456.begin().lattice()));
}
TEST_FIXTURE(LatticeFixture, bcc_iterator_3) {
    CHECK_EQUAL(std::get<0>(l456.index({0u,0u,0u})), std::get<0>(l456.begin().index()));
    CHECK_EQUAL(std::get<1>(l456.index({0u,0u,0u})), std::get<1>(l456.begin().index()));
}

TEST_FIXTURE(LatticeFixture, bcc_coord_iterator_1) {
    for(auto i=l456.coord_begin(); i!=l456.coord_end(); ++i) {
        auto c = i.coordinate();
        CHECK_EQUAL(std::get<0>(l456.index(c)), std::get<0>(i.index()) ) ;
        CHECK_EQUAL(std::get<1>(l456.index(c)), std::get<1>(i.index()) ) ;
    }
}


