#include <unittest++/UnitTest++.h>

#include <cmath>  // sin(), cos()
#include "../spin/heisenberg_spherical.h"


TEST(HeisenbergSphericalInitRandom_1) {
    typedef spin::HeisenbergSphericalInitRandom<float, 2> Init;
    auto angles = Init::spin_init({{1.2}});
    CHECK_CLOSE(2*M_PI * 1.2, angles[0], 0.001);
}
TEST(HeisenbergSphericalInitRandom_2) {
    typedef spin::HeisenbergSphericalInitRandom<float, 3> Init;
    auto angles = Init::spin_init({{1.2, 3.4}});
    CHECK_CLOSE(1*M_PI * 1.2, angles[0], 0.001);
    CHECK_CLOSE(2*M_PI * 3.4, angles[1], 0.001);
}
TEST(HeisenbergSphericalInitRandom_3) {
    typedef spin::HeisenbergSphericalInitRandom<float, 4> Init;
    auto angles = Init::spin_init({{1.2, 3.4, 5.6}});
    CHECK_CLOSE(1*M_PI * 1.2, angles[0], 0.001);
    CHECK_CLOSE(2*M_PI * 3.4, angles[1], 0.001);
    CHECK_CLOSE(2*M_PI * 5.6, angles[2], 0.001);
}

TEST(HeisenbergSphericalInitZ_1) {
    typedef spin::HeisenbergSphericalInitZ<float, 2> Init;
    auto angles = Init::spin_init({{}});
    CHECK_EQUAL(0, angles[0]);
}
TEST(HeisenbergSphericalInitZ_2) {
    typedef spin::HeisenbergSphericalInitZ<float, 3> Init;
    auto angles = Init::spin_init({{1.}});
    CHECK_EQUAL(0, angles[0]);
    CHECK_CLOSE(2*M_PI, angles[1], 0.001);
    angles = Init::spin_init({{2.}});
    CHECK_CLOSE(2*M_PI * 2., angles[1], 0.001);
}
TEST(HeisenbergSphericalInitZ_3) {
    typedef spin::HeisenbergSphericalInitZ<float, 4> Init;
    auto angles = Init::spin_init({{1.1, 1.2}});
    CHECK_EQUAL(0, angles[0]);
    CHECK_CLOSE(2*M_PI * 1.1, angles[1], 0.001);
    CHECK_CLOSE(2*M_PI * 1.2, angles[2], 0.001);
}

TEST(HeisenbergSphericalOperations_magnetisation_1) {
    constexpr size_t N = 2;
    typedef std::array<float, N-1> spin_type;
    typedef spin::HeisenbergSphericalOperations<spin_type,N> Heis;
    
    spin_type spin {{1.2}};
    std::array<double, N> M = Heis::magnetisation(spin);
    CHECK_CLOSE(std::sin(spin[0]), M[0], 0.001);
    CHECK_CLOSE(std::cos(spin[0]), M[1], 0.001);
}
TEST(HeisenbergSphericalOperations_magnetisation_2) {
    constexpr size_t N = 3;
    typedef std::array<float, N-1> spin_type;
    typedef spin::HeisenbergSphericalOperations<spin_type,N> Heis;
    
    spin_type spin {{1.2, 3.4}};
    std::array<double, N> M = Heis::magnetisation(spin);
    CHECK_CLOSE(std::sin(spin[0])*std::sin(spin[1]), M[0], 0.001);
    CHECK_CLOSE(std::sin(spin[0])*std::cos(spin[1]), M[1], 0.001);
    CHECK_CLOSE(std::cos(spin[0]), M[2], 0.001);
}

TEST(HeisenbergSphericalOperations_bond_energy_1) {
    constexpr size_t N = 2;
    typedef std::array<float, N-1> spin_type;
    typedef spin::HeisenbergSphericalOperations<spin_type,N> Heis;
    
    spin_type spin1 {{1.2}};
    spin_type spin2 {{5.2}};
    float g = 0.2;
    std::array<double, N> M0 = Heis::magnetisation(spin1);
    std::array<double, N> M1 = Heis::magnetisation(spin2);
    double H = Heis::bond_energy(spin1, spin2, g);
    double H_expect = M0[0]*M1[0] + M0[1]*M1[1]*(1+g);
    CHECK_CLOSE(H_expect, H, 0.001);
}
TEST(HeisenbergSphericalOperations_bond_energy_2) {
    constexpr size_t N = 3;
    typedef std::array<float, N-1> spin_type;
    typedef spin::HeisenbergSphericalOperations<spin_type,N> Heis;
    
    spin_type spin1 {{1.2, 3.4}};
    spin_type spin2 {{5.2, -6.4}};
    float g = 0.2;
    std::array<double, N> M0 = Heis::magnetisation(spin1);
    std::array<double, N> M1 = Heis::magnetisation(spin2);
    double H = Heis::bond_energy(spin1, spin2, g);
    double H_expect = M0[0]*M1[0] + M0[1]*M1[1] + M0[2]*M1[2]*(1+g);
    CHECK_CLOSE(H_expect, H, 0.001);
}
