#include <unittest++/UnitTest++.h>

#include <array>
#include <vector>
#include <set>
#include <algorithm>
#include <numeric>
#include <stdexcept>        // out_of_range
#include "../RNG/ran1.h"
#include "../RNG/rng_sequence.h"
#include "../lattice/sc.h"
#include "../spin/heisenberg_spherical.h"

typedef spin::Heisenberg<
            3, float, 
            spin::HeisenbergSpherical,			// Spherical coordinate system
            spin::HeisenbergSphericalInitRandom, 
            spin::HeisenbergSphericalOperations3N
        > Spin;

typedef lattice::SimpleCubic<3, rng::Ran1>::type<Spin::type> Lattice;
typedef lattice::SimpleCubic<2, rng::Ran1>::type<Spin::type> Latticex2;
typedef lattice::SimpleCubic<1, rng::Ran1>::type<Spin::type> Latticex1;

struct LatticeFixture {
    Lattice l456{{4u,5u,6u}, 1234};
    Latticex2 l45{{4u,5u}, 1234};
    Latticex1 l4{{4u}, 1234};
};

TEST_FIXTURE(LatticeFixture, lattice_index_1) {
    CHECK_EQUAL(0, l456.index({0u,0u,0u}));
}
TEST_FIXTURE(LatticeFixture, lattice_index_2) {
    CHECK_EQUAL(3, l456.index({3u,0u,0u}));
}
TEST_FIXTURE(LatticeFixture, lattice_index_3) {
    CHECK_EQUAL(4, l456.index({0u,1u,0u}));
}
TEST_FIXTURE(LatticeFixture, lattice_index_4) {
    CHECK_EQUAL(6, l456.index({2u,1u,0u}));
}
TEST_FIXTURE(LatticeFixture, lattice_index_5) {
    CHECK_EQUAL(20, l456.index({0u,0u,1u}));
}
TEST_FIXTURE(LatticeFixture, lattice_index_6) {
    CHECK_EQUAL(1+4+4*5, l456.index({1u,1u,1u}));
}
TEST_FIXTURE(LatticeFixture, lattice_index_7) {
    CHECK_EQUAL(2+2*4+2*4*5, l456.index({2u,2u,2u}));
}
TEST_FIXTURE(LatticeFixture, lattice_index_8) {
    CHECK_EQUAL(3+3*4+3*4*5, l456.index({3u,3u,3u}));
}
TEST_FIXTURE(LatticeFixture, lattice_index_9) {
    CHECK_EQUAL(3+4*4+5*4*5, l456.index({3u,4u,5u}));
}

TEST_FIXTURE(LatticeFixture, at_1) {
    CHECK_THROW(l456.at(-1), std::out_of_range);
}
TEST_FIXTURE(LatticeFixture, at_2) {
    CHECK_THROW(l456.at(4*5*6), std::out_of_range);
}
TEST_FIXTURE(LatticeFixture, at_3) {
    CHECK_THROW(const_cast<const Lattice&>(l456).at(-1), std::out_of_range);
}
TEST_FIXTURE(LatticeFixture, at_4) {
    CHECK_THROW(const_cast<const Lattice&>(l456).at(4*5*6), std::out_of_range);
}

TEST_FIXTURE(LatticeFixture, index_moduoN_1) {
    CHECK_EQUAL(l456.index({0u,0u,0u}), l456.index_moduoN({4u,0u,0u}) );
}
TEST_FIXTURE(LatticeFixture, index_moduoN_2) {
    CHECK_EQUAL(l456.index({0u,1u,0u}), l456.index_moduoN({4u,1u,0u}) );
}
TEST_FIXTURE(LatticeFixture, index_moduoN_3) {
    CHECK_EQUAL(l456.index({4-2u,1u,0u}), l456.index_moduoN({-2u,1u,0u}) );
}
TEST_FIXTURE(LatticeFixture, index_moduoN_4) {
    CHECK_EQUAL(l456.index({0u,5-1u,0u}), l456.index_moduoN({0u,-1u,0u}) );
}
TEST_FIXTURE(LatticeFixture, index_moduoN_5) {
    CHECK_EQUAL(l456.index({0u,0u,6-1u}), l456.index_moduoN({0u,0u,-1u}) );
}
TEST_FIXTURE(LatticeFixture, index_moduoN_6) {
    CHECK_EQUAL(l456.index({5%4u,0u,6-1u}), l456.index_moduoN({5u,0u,-1u}) );
}
TEST_FIXTURE(LatticeFixture, sc_lattice_index_moduoN_1) {
    CHECK_EQUAL((4-1u), l4.index_moduoN({-1u}) );
}
TEST_FIXTURE(LatticeFixture, sc_lattice_index_moduoN_2) {
    CHECK_EQUAL((4-4u), l4.index_moduoN({4u}) );
}
TEST_FIXTURE(LatticeFixture, sc_lattice_index_moduoN_3) {
    CHECK_EQUAL((4-1u)+(5-1u)*4, l45.index_moduoN({-1u, -1u}) );
}

////////////////////////
template<typename T>
std::ostream & operator<<(std::ostream & out, const std::set<T> & s) {
    for(auto & i : s) out << i << ",";
    return out;
}
template<typename T>
std::ostream & operator<<(std::ostream & out, const std::vector<T> & s) {
    for(auto & i : s) out << i << ",";
    return out;
}



TEST_FIXTURE(LatticeFixture, neighbor_indices_a) {
    auto ni_ = l456.neighbor_indices({0u,0u,0u});
    std::set<unsigned> ni(ni_.begin(), ni_.end());
    std::set<unsigned> expected {1, 3, 4, 16, 20, 100};
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, neighbor_indices_b) {
    auto ni_ = l456.neighbor_indices({3u,0u,0u});
    std::set<unsigned> ni(ni_.begin(), ni_.end());
    std::set<unsigned> expected {0, 2, 7, 19, 23, 103};
    CHECK_EQUAL(expected, ni); 
}

TEST_FIXTURE(LatticeFixture, neighbor_indices__1___1) {
    auto ni_ = l456.neighbor_indices<1>({0u,0u,0u});
    std::set<unsigned> ni(ni_.begin(), ni_.end());
    std::set<unsigned> expected {1, 3, 4, 16, 20, 100};
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, neighbor_indices__1___2) {
    auto ni_ = l456.neighbor_indices<1>({3u,0u,0u});
    std::set<unsigned> ni(ni_.begin(), ni_.end());
    std::set<unsigned> expected {0, 2, 7, 19, 23, 103};
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, neighbor_indices_1___3) {
    for(auto coord_it = l456.coord_begin(); coord_it!=l456.end(); ++coord_it) {
        for(auto i : l456.neighbor_indices(coord_it.coordinates())) {
            CHECK_EQUAL(l456[i], l456.at(i)); 
        }
    }
}

TEST_FIXTURE(LatticeFixture, neighbor_indices__2___1) {
    auto ni_ = l456.neighbor_indices<2>({0u,0u,0u});
    std::set<unsigned> ni(ni_.begin(), ni_.end());
    //   Mx,My,z;  Mx,y,Mz;  x,My,Mz;
    std::set<unsigned> expected {
        1+1*4+0*4*5, 1+4*4+0*4*5, 3+1*4+0*4*5, 3+4*4+0*4*5, 
        1+0*4+1*4*5, 1+0*4+5*4*5, 3+0*4+1*4*5, 3+0*4+5*4*5, 
        0+1*4+1*4*5, 0+1*4+5*4*5, 0+4*4+1*4*5, 0+4*4+5*4*5 
    };
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, neighbor_indices__2___2) {
    auto ni_ = l456.neighbor_indices<2>({3u,0u,0u});
    std::set<unsigned> ni(ni_.begin(), ni_.end());
    //   Mx,My,z;  Mx,y,Mz;  x,My,Mz;
    std::set<unsigned> expected {
        0+1*4+0*4*5, 0+4*4+0*4*5, 2+1*4+0*4*5, 2+4*4+0*4*5, 
        0+0*4+1*4*5, 0+0*4+5*4*5, 2+0*4+1*4*5, 2+0*4+5*4*5, 
        3+1*4+1*4*5, 3+1*4+5*4*5, 3+4*4+1*4*5, 3+4*4+5*4*5 
    };
    CHECK_EQUAL(expected, ni); 
}
TEST_FIXTURE(LatticeFixture, neighbor_indices__2___3) {
    auto ni_ = l456.neighbor_indices<2>({0u,0u,5u});
    std::set<unsigned> ni(ni_.begin(), ni_.end());
    //   Mx,My,z;  Mx,y,Mz;  x,My,Mz;
    std::set<unsigned> expected {
        1+1*4+5*4*5, 1+4*4+5*4*5, 3+1*4+5*4*5, 3+4*4+5*4*5, 
        1+0*4+0*4*5, 1+0*4+4*4*5, 3+0*4+0*4*5, 3+0*4+4*4*5, 
        0+1*4+0*4*5, 0+1*4+4*4*5, 0+4*4+0*4*5, 0+4*4+4*4*5 
    };
    CHECK_EQUAL(expected, ni); 
}

////////////////////////

TEST_FIXTURE(LatticeFixture, sc_iterator_1) {
    std::vector<size_t> idxs;
    for(auto i=l456.begin(); i!=l456.end(); ++i) {
        idxs.push_back(i.index());
    }
    std::vector<size_t> expected(idxs.size());
    std::iota(expected.begin(), expected.end(), 0);
    CHECK_EQUAL(expected, idxs); 
}
TEST_FIXTURE(LatticeFixture, sc_iterator_2) {
    CHECK_EQUAL(&l456, &(l456.begin().lattice()));
}
TEST_FIXTURE(LatticeFixture, sc_iterator_3) {
    CHECK_EQUAL(l456.index({0u,0u,0u}), l456.begin().index());
//    CHECK_EQUAL(std::get<1>(l456.index({0u,0u,0u})), std::get<1>(l456.begin().index()));
}

TEST_FIXTURE(LatticeFixture, coord_iterator_1) {
    Lattice::coordinate_type expected{0u,0u,0u};
    auto i = l456.coord_begin();
    CHECK_EQUAL(expected, i.coordinates()); 
}
TEST_FIXTURE(LatticeFixture, coord_iterator_2) {
    Lattice::coordinate_type expected{3u,4u,5u};
    auto it = l456.coord_begin();
    for(int i=0; i<4*5*6-1; ++i) {
        ++it;
    }
    CHECK_EQUAL(expected, it.coordinates()); 
}
TEST_FIXTURE(LatticeFixture, coord_iterator_3) {
    Lattice::coordinate_type expected{3u,4u,5u};
    for(auto it=l456.coord_begin(); it!=l456.coord_end(); ++it) {
        CHECK_EQUAL(it.index(), it.lattice().index(it.coordinates()) ); 
    }
}

