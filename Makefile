# hypermo/

.PHONY: help include install uninstall

help:
	@echo "Usage: make <target>"
#	@echo ""
	@echo "Targets:"
	@echo "        - help"
	@echo "        - include"
	@echo "        - install"
	@echo "        - doc"

PROJECT_NAME=hypermo
DIRS_TO_INCLUDE_FROM=util lattice spin RNG
H_FILES=$(wildcard $(addsuffix /*.h,$(DIRS_TO_INCLUDE_FROM)))

include: hypermo.h

hypermo.h:
	for i in $(H_FILES); do echo "#include<$(PROJECT_NAME)/$$i>"; done > $@

ROOT=/usr/local/
INCLUDE_DIR=$(ROOT)/include/
DOC_DIR=$(ROOT)/share/doc/
HYPERMO_INCLUDE_DIR=$(INCLUDE_DIR)/hypermo/
HYPERMO_DOC_DIR=$(DOC_DIR)/hypermo/
DIRS_TO_DOCUMENT_FROM=simulations test doc
INSTALL_DIRS1=$(addprefix $(HYPERMO_INCLUDE_DIR)/,$(DIRS_TO_INCLUDE_FROM) )
INSTALL_DIRS2=$(addprefix $(HYPERMO_DOC_DIR)/,$(DIRS_TO_DOCUMENT_FROM))

install: hypermo.h
	install -d $(INSTALL_DIRS1) $(INSTALL_DIRS2)
	#
	for i in $(H_FILES); do install --mode=644 $$i $(HYPERMO_INCLUDE_DIR)/$$i; done 
	install --mode=644 hypermo.h $(HYPERMO_INCLUDE_DIR)/
	#
	install --mode=644 simulations/*.h simulations/*.cpp $(HYPERMO_DOC_DIR)/simulations/
	install --mode=644 simulations/Makefile $(HYPERMO_DOC_DIR)/simulations/
	echo "\nINCLUDE_DIR=-I $(INCLUDE_DIR)" >> $(HYPERMO_DOC_DIR)/simulations/Makefile
	#
	install --mode=644 test/*.cpp $(HYPERMO_DOC_DIR)/test/
	#
	cp -r doc/*  $(HYPERMO_DOC_DIR)/doc/

uninstall:
	rm -fr $(HYPERMO_INCLUDE_DIR) $(HYPERMO_DOC_DIR)


doc:
	doxygen
